%! TEX root = 00.tex
Formal presentation of the algorithm that compiles an LL(1) grammar into an implementation of a
  language recognizer with \Java generics
  is delayed to~\cref{section:algorithm}.
This section gives an intuitive perspective on this algorithm.

We will first recall the essentials of the classical LL(1) parsing algorithm (\cref{section:essentials}).
Then, we will explain the limitations of the computational model
  offered by \Java generics (\cref{section:limitations}).

The discussion proceeds to the observation
  that underlies our emulation of the parsing algorithm
  within these limitations.

Building on all these, \Cref{section:generation} can
make the intuitive introduction to the main algorithm.
To this end, we revise here the classical
algorithm~\cite{Lewis:66} for converting an LL grammar
(in writing LL, we, here and henceforth really mean
LL(1)), and explain how it is modified to generate a
  recognizer executable on the computational model of
\Java generics.

\subsection{Essentials of LL parsing}
\label{section:essentials}
The traditional \emph{\textbf{LL} \textbf Parser} (\LLp)
  is a DPDA allowed to peek at the next input symbol.
Thus,~$δ$, its transition function, takes two parameters: the current
  state of the automaton, and a peek into the terminal next to be read.
The actual operation of the automaton is by executing in loop the step
  depicted in \cref{algorithm:ll-parser}.

\renewcommand\algorithmicdo{\textbf{\emph{}}}
\renewcommand\algorithmicthen{\textbf{\emph{}}}
\begin{algorithm}
  \caption{\label{algorithm:ll-parser}
  The iterative step of an \LLp}
  \begin{algorithmic}[1]
      \LET{$X$}{\Function pop()}\COMMENT{what's next to parse?}
      \IF[anticipate to match terminal~$X$]{$X∈Σ$}
        \IF[read terminal is not anticipated~$X$]{$X≠\Function next()$}
          \STATE{\textsc{Reject}}\COMMENT{automaton halts in error}
        \ELSE[terminal just read was the anticipated~$X$]
          \STATE{\textsc{Continue}}\COMMENT{restart, popping a new~$X$, etc.}
        \FI
      \ELSIF[anticipating end-of-input]{$X=\$$}
        \IF[not the anticipated end-of-input]{$\$≠\Function next()$}
          \STATE{\textsc{Reject}}\COMMENT{automaton halts in error}
        \ELSE[matched the anticipated end-of-input]
          \STATE{\textsc{Accept}}\COMMENT{all input successfully consumed}
        \FI
      \ELSE[anticipated~$X$ must be a nonterminal]
        \LET{$R$}{$\Function δ(X, \Function peek())$}\COMMENT{determine rule to parse~$X$}
        \IF[no rule found]{$R=⊥$}
          \STATE{\textsc{Reject}}\COMMENT{automaton halts in error}
        \ELSE[A rule was found]
          \LET{$(Z ::= Y₁,…,Yₖ)$}{R}\COMMENT{break into left/right}
          \STATE{\textbf{assert}~$Z=X$}\COMMENT{$δ$ constructed to return valid~$R$ only}
          \STATE{$\Function print(R)$}\COMMENT{rule~$R$ has just been applied}
          \STATE{\textbf{For}~$i=k,…,1$,~$\Function push(Yᵢ)$}\COMMENT{push in reverse order}
          \STATE{\textsc{Continue}}\COMMENT{restart, popping a new~$X$, etc.}
        \FI % No rule found
      \FI % Main
\end{algorithmic}
  \vspace{0.3ex}
  \hrule
  \vspace{0.3ex}

  \ReplaceInThesis{\scriptsize}{}
  \begin{enumerate}
      \item
  Input is a string of terminals drawn from alphabet~$Σ$, ending
    with a special symbol~$\$∉Σ$.
      \item
  Stack symbols are drawn from~$Σ∪❴\$❵∪Ξ$, where~$Ξ$ is
  the set of nonterminals of the grammar from which the automaton was generated.
\item
  Functions~$\Function pop(·)$ and~$\Function push(·)$
    operate on the pushdown stack; function~$\Function next()$ returns
  and consumes the next terminal from the input string;
    function~$\Function peek()$ returns this terminal without consuming it.
\item
  The automaton starts with the stack with the start symbol~$S∈Ξ$ pushed
    into its stack.
  \end{enumerate}
\end{algorithm}

The DPDA maintains a stack of ‟anticipated” symbols, which may
  be of three kinds: a terminal drawn from the input alphabet~$Σ$,
  an end-of-input symbol~$\$$, or one of~$Ξ$, the set of
  nonterminals of the underlying grammar.

If the top of the stack is an input symbol or the special,
 end-of-input symbol~$\$$, then it must match the next terminal
 in the input string, the matched terminal is then consumed from
 the input string.
If there is no match, the parser rejects.
The parser accepts if the input is exhausted with
  no rejections (i.e.,~$\$$ was matched).

The more interesting case is that~$X$, the popped symbol
  is a nonterminal: the DPDA peeks into the next terminal in the input
  string (without consuming it).
Based on this terminal, and~$X$, the transition function~$δ$
  determines~$R$-the derivation rule applied to derive~$X$.
The algorithm rejects if~$δ$ can offer no such rule.
Otherwise, it pushes into the stack, in reverse order, the symbols
  found in the right hand side of~$R$.

\subsection{LL(1) Parsing with \Java Generics?}
\label{section:limitations}
Can \cref{algorithm:ll-parser} be executed on the machinery
  available with \Java generics?
As it turns out, most operations conducted by the algorithm
  are easy.
The implementation of function~$δ$ can
  be found in the toolbox in \cite{Gil:Levy:2016}.
Similarly, any fixed sequence of push and pop
  operations on the stack can be conducted within a \Java
  transition function:
  the ‟\textbf{For}" at the algorithm can be unrolled.

Superficially, the algorithm appears to be doing a constant amount
  of work for each input symbol.
A little scrutiny falsifies such a conclusion.

In the case~$R=X::=Y$,~$Y$ 
  being a nonterminal, the algorithm will conduct
  a~$\Function pop(·)$ to remove~$X$ and a~$\Function push(·)$
  operation for~$Y$ before consuming a terminal from the input.
Further,~$δ$ may return next the rule~$Y::=Z$,
  and then the rule~$Z::=Z'$, etc.
Let~$k'$ be the number of such substitutions
  occurring while reading a single input token.

\begin{Definition}[$k'$ - subtitution factor]
  \label{substitution-factor}
  Let~$A$ be a nonterminal at the top of the stack
    and~$t$ be the next input symbol.
  If~$t \in \Function First(A)$ then~$k'$ is the number of 
  consecutive substitutions the parser will perform 
  (replacing the nonterminal at the top the stack with one of it's rules)
  until~$t$ will be consumed from the input.
\end{Definition}

Also, in the case~$R=X::=ε$ the DPDA does not push
  anything into the stack.
Further, in its next iteration, the DPDA conducts another
pop,
\[
  X'←\Function pop(),
\]
instruction and proceeds to consider this new~$X'$.
If it so happens, it could also be the case
  that the right hand side of rule~$R'$
  \[
    R' = δ(X', \Function peek())
  \]
  is once again empty,
  and then another~$\Function pop()$
    instruction occurs
\[
  X”←\Function pop(),
\]
  etc.
Let~$k^*$ be the number
  of such instructions in a certain such mishap.

\begin{Definition}[$k^*$ - pop factor]
  \label{pop-factor}
  Let~$X$ be a nonterminal at the top of the stack
    and~$t$ be the next input symbol.
  Then~$k^*$ is the number of consecutive substitutions
  the parser will perform \emph{of only~$ε$ rules}
  until~$t$ will be consumed from the input.
\end{Definition}

The cases~$k' > 0$ and~$k^* > 0$ are not rarities.
For example, let us concentrate on the grammar
  depicted in \cref{figure:running}.
This grammar, inspired by the prototypical
  specification of \Pascal \urlref{http://www.fit.vutbr.cz/study/courses/APR/public/ebnf.html},
  shall serve as running example.

\newsavebox{\Alphabet}
\begin{lrbox}{\Alphabet}
  \begin{tabularx}{0.40\linewidth}{l}
    \cc{program}, \cc{begin}, \cc{end},⏎
    \cc{label}, \cc{const}, \cc{id},⏎
    \cc{procedure}, \cc{;}, \cc{()}
  \end{tabularx}
\end{lrbox}

\begin{figure}[!htbp]
  \caption[An LL(1) grammar (fragment of the \Pascal's grammar)]{\label{figure:running}
    An LL(1) grammar over the alphabet
    \[
      Σ = \left❴\usebox\Alphabet\right❵.
    \]
    (inspired by the original \Pascal grammar; to serve as
    our running example)
  }
  \begin{Grammar}
    \begin{aligned}
      \<Program> & \Derives \cc{program} \cc{id} \<Parameters>\~\cc{;} \<Definitions> \<Body>\hfill⏎
      \<Body> & \Derives \cc{begin} \cc{end}\hfill⏎
      \<Definitions> & \Derives \<Labels>\~\<Constants>\~\<Nested>\hfill⏎
      \<Labels> & \Derives ε \| \cc{label} \<Label>\~\<MoreLabels> \hfill⏎
      \<Constants> & \Derives ε \| \cc{const} \<Constant>\~\<MoreConstants> \hfill⏎
      \<Label> & \Derives\cc{;} \hfill⏎
      \<Constant> & \Derives\cc{;} \hfill⏎
      \<MoreLabels> & \Derives ε \| \<Label>\~\<MoreLabels>\hfill⏎
      \<MoreConstants> & \Derives ε \| \<Constant>\~\<MoreConstants>\hfill⏎
      \<Nested> & \Derives ε \| \<Procedure>\~\<Nested> \hfill⏎
      \<Procedure> & \Derives \cc{procedure}\cc{id}\<Parameters>\~\cc{;}\<Definitions>⏎
                    & \<Body> \hfill⏎
      \<Parameters> & \Derives ε \| \cc{()} \hfill⏎
    \end{aligned}
  \end{Grammar}
\end{figure}

The grammar preserves the ‟theme” of nested definitions of \Pascal, while
trimming these down as much as possible. \cref{table:derived-strings}
presents some legal sequences derived by this grammar.

\begin{table}[H]
  \caption{\label{table:derived-strings}
      Some legal words in the language defined by \cref{figure:running}}
  \rowcolors{2}{}{olive!20}
  \centering
  \begin{tabular}{m{58ex}}
    \toprule
      \cc{program id ; begin end}⏎
      \cc{program id () ; label ; begin end}⏎
      \cc{program id () ; label ; ; ; ; const ; begin end}⏎
      \cc{program id ; procedure id ; procedure id ;} \newline
          \cc{begin end begin end begin end}⏎
    \bottomrule
  \end{tabular}
\end{table}
In order to demonstrate the problems with~$k'$ and~$k^*$ we first
  supply the~$\Function First(·)$, and~$\Function Follow(·)$
  sets of our example grammar.

The~$\Function First(·)$ set of symbol~$X$ is the set of all terminals
that might appear at the beginning of a string derived from~$X$ (algorithm for
computing~$\Function First(·)$ is presented in \cref{algorithm:first}),
thus, for example, if~$X$ is a terminal then its first set is the set
containing only~$X$.
The~$\Function First(·)$ set is extended for strings too,~$\Function First(α)$
contains all terminals that can begin a string derived from~$α$ (Appropriate
algorithm can be found at~\cref{algorithm:first-string}).

The~$\Function Follow(·)$ set of nonterminal~$A$ is the set of all
  terminals that might appear immediately after a string that was
  derived from~$A$ (Appropriate algorithm can be found
  at~\cref{algorithm:follow}).

\begin{algorithm}[H]
  \caption{\label{algorithm:first}
  An algorithm for computing~$\Function First(X)$ for each grammar symbol~$X$
    in the input grammar~$G =⟨Σ,Ξ,P⟩$}
    \begin{algorithmic}
    \FOR[initialize~$\Function First(·)$ for all nonterminals]{$A∈Ξ$}
      \STATE{$\Function First(A)=∅$}
    \ENDFOR
    \FOR[initialize~$\Function First(·)$ for all terminals]{$t∈Σ$}
      \STATE{$\Function First(t)=❴t❵$}
    \ENDFOR
    \WHILE{No changes in any~$\Function First(·)$ set}
      \FOR[for every production]{$X::=Y₀…Yₖ∈P$}
        \STATE{$\Function First(X) \; ∪\! = \Function First(Y₀)$}
        \COMMENT{add~$\Function First(Y₀)$ to~$\Function First(X)$}
        \FOR[for each symbol in the RHS]{$i∈❴0…k❵$}
          \IF[if the prefix is nullable]{$\Function Nullable(Y₀…Y_{i-1})$}
          \STATE{$\Function First(X) \; ∪\! = \Function First(Yᵢ)$}
            \COMMENT{add~$\Function First(Yᵢ)$ to~$\Function First(X)$}
          \ENDIF
        \ENDFOR % Symbols in the right-hand-side of a production
      \ENDFOR % Productions
    \ENDWHILE
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}
  \caption{\label{algorithm:first-string}
  An algorithm for computing~$\Function First(α)$ for some string of symbols~$α$.
  This algorithm relies on results from~\cref{algorithm:first}}
  \begin{algorithmic}
    \LET{$Y₀…Yₖ$}{$α$}\COMMENT{break~$α$ into its symbols}
    \STATE{$\Function First(α) \; ∪\! = \Function First(Y₀)$}
    \COMMENT{initialize~$\Function First(α)$ with~$\Function First(Y₀)$}
    \FOR[for each symbol in the string]{$i∈❴1…k❵$}
      \IF[if the prefix is nullable]{$\Function Nullable(Y₀…Y_{i-1})$}
        \STATE{$\Function First(α) \; ∪ \! = \Function First(Yᵢ)$}
        \COMMENT{add~$\Function First(Yᵢ)$ to~$\Function First(α)$}
      \ENDIF
    \ENDFOR
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}
  \caption{\label{algorithm:follow}
  An algorithm for computing~$\Function Follow(A)$ for each nonterminal~$X$
    in the input grammar~$G =⟨Σ,Ξ,P⟩$ when~$\$∉Σ$
    and~$S∈Σ$ is the start symbol of~$G$}
  \begin{algorithmic}
    \STATE{$\Function Follow(S)=❴ \$ ❵$}
    \COMMENT{initialize the start symbol}
    \WHILE{No changes in any~$\Function Follow(·)$ set}
      \FOR[for each grammar rule]{$A::=Y₀…Yₖ∈P$}
        \FOR[for each symbol in the RHS]{$i∈❴0…k❵$}
          \IF[compute only for nonterminals]{$Yᵢ∉Ξ$}
            \CONTINUE
          \ENDIF
          \STATE{$\Function Follow(Yᵢ) \; ∪ \! = \Function First(Y_{i+1}…Yₖ)$}
          \IF[if the suffix is nullable]{$\Function Nullable(Y_{i+1}…Yₖ)$}
            \STATE{$\Function Follow(Yᵢ) \, ∪ \! = \Function First(A)$}
            \COMMENT{add~$\Function First(A)$}
          \ENDIF
        \ENDFOR % Symbol in the RHS
      \ENDFOR
    \ENDWHILE
  \end{algorithmic}
\end{algorithm}

For example, the nonterminal \<Definitions> can be derived to a string
  beginning with \cc{label} (if there are defined labels), or \cc{const}
  (if there are no labels defined), or \cc{procedure} (if there are no
  labels or constants defined).
$\Function Follow(\<Definitions>)$ on the other hand, contains only
\cc{begin} since~\<Definitions> is always followed by~\<Body>,
  and~$\Function First(\<Body>)$ contains only \cc{begin}.

The~$\Function First(·)$ and~$\Function Follow(·)$ sets for
  our running example (\cref{figure:running}) are listed in~\cref{table:running-first-follow}.
\begin{table}[ht]
  \caption{\label{table:running-first-follow}
    Sets~$\Function First(·)$ and~$\Function Follow(·)$ for nonterminals of
    the grammar in~\cref{figure:running}}
    \rowcolors{2}{}{olive!20}
    \centering
    \ReplaceInThesis{
      \begin{tabular}{m{16ex}m{14ex}m{19ex}}
    }{
      \begin{tabular}{m{0.21\linewidth}m{0.32\linewidth}m{0.37\linewidth}}
    }
      \toprule
    Nonterminal & $\Function First(·)$ & $\Function Follow(·)$⏎
      \midrule
    \<Program> & \cc{program} & $∅$⏎
    \<Labels> & \cc{label} & \cc{const}, \cc{procedure}, \ReplaceInThesis{\newline}{} \cc{begin}⏎
    \<Constants> & \cc{constant} & \cc{procedure}, \cc{begin}⏎
    \<Label> & \cc{;} & \cc{;}, \cc{const}, \ReplaceInThesis{\newline}{} \cc{procedure}, \ReplaceInThesis{\newline}{} \cc{begin}⏎
    \<MoreLabels> & \cc{;} & \cc{const}, \cc{procedure}, \ReplaceInThesis{\newline}{} \cc{begin}⏎
    \<Constant> & \cc{;} & \cc{;}, \cc{procedure}, \ReplaceInThesis{\newline}{} \cc{begin}⏎
    \<MoreConstants> & \cc{;} & \cc{procedure}, \cc{begin}⏎
    \<Nested> & \cc{procedure} & \cc{begin}⏎
    \<Procedure> & \cc{procedure} & \cc{procedure}, \cc{begin}⏎
    \<Definitions> & \cc{label}, \cc{const}, \cc{procedure}& \cc{begin}⏎
    \<Body> & \cc{begin} & \cc{procedure}, \cc{begin}⏎
    \<Parameters> & \cc{()} & \cc{;}⏎
      \bottomrule
  \end{tabular}
\end{table}

In our example, nonterminal \<Procedure> has only one derivation
  rule, which begins with the terminal \cc{procedure}.
Thus, all derivations of \<Procedure> must begin with terminal
  \cc{procedure} and the~$\Function First(·)$ set of \<Procedure> is
  contains only~\cc{procedure}, as can be seen
  in~\cref{table:running-first-follow}.
The~$\Function Follow(·)$ set of \<Procedure> contains two
  terminals.
The first is \cc{procedure}, because the nonterminal \<Procedure>
  is followed by \<Nested> in \<Nested>'s rule,
  and \<Nested> can begin only with \cc{procedure}.
The second is \cc{begin}, because \<Nested> is nullable, and
  \cc{begin} appears in the~$\Function Follow(·)$ set of \<Nested>.

\subsubsection{Examples for~\texorpdfstring{$k'$}{k'} for the \Pascal grammar fragment in~\texorpdfstring{\cref{figure:running}}{}}
Consider a state of the parser, in which~$\<Definitions>$ is on
  the top of the top of the stack, and the next token on the input string
  is \cc{label}.
Until that token is consumed, the parser will:
  \begin{enumerate}
    \item pop~$\<Definitions>$ from the stack, and push the
      nonterminals~$\<Labels>$,~$\<Constants>$, and~$\<Nested>$
      in reversed order.
    \item pop~$\<Labels>$ and push the symbols~$\cc{label}$,~$\<Label>$,
      and~$\<MoreLabels>$ in reversed order.
    \item match the top of the stack~\cc{label} with the input
      symbol~\cc{label} and consume it.
  \end{enumerate}
Since we first substituted~$\<Definitions>$, then~$\<Labels>$
  and only then consumed the input symbol,~$k'=2$.

The problem is that every such operation is an~$ε$-move of the
  parser's DPDA, and as was shown in~\cite{Gil:Levy:2016} it causes
  problems when we want to employ \Java's compiler to ‟run” our automaton.

Another case is when encountering~$\<Nested>$ on the top of the stack
  and \cc{procedure} in the input string.
$k'=2$ again, where at first the non-$ε$ rule of~$\<Nested>$ will
  be pushed, then the non-$ε$ rule of~$\<Procedure>$ will be
  pushed, and only then will the terminal~\cc{procedure} will match,
  and be consumed from the input string.

\subsubsection{Examples of \texorpdfstring{$k^*$}{k*} for the \Pascal grammar fragment in \texorpdfstring{\cref{figure:running}}{}}
Consider a state in which the parser is in, where the
  only rule of~$\<Program>$ is being processed,~$\<Definitions>$
  was just replaced on the stack by it's only rule, and~$\<Body>$ is below.
Assume also, that the first terminal in the input string is \cc{begin}.

Until the next input token will be consumed, the following will happen:
  \begin{enumerate}
    \item consulting with the transition
      function~$δ$,~$\<Labels>$'s~$ε$-rule will
      be chosen,
      thus,~$\<Labels>$ will be popped from the stack.
    \item the same will happen with~$\<Constants>$
      and~$\<Nested>$.
    \item only after seeing nonterminal~$\<Body>$ shall the right rule will be
      pushed, and the token matched.
  \end{enumerate}
In this case,~$k^*=3$ because we had to pop three nonterminals and
  ‟replace” them with their matching rules, that are all~$ε$-rules
  before we got to a nonterminal that will derive to something other
  then~$ε$.

In the last example~$k^*$ was determined by the size of~$\<Definitions>$, but that
  doesn't have to be the case all the time.
For example, if in our grammar, in~$\<Program>$'s rule,
  nonterminal~$\<Parameters>$ would follow~$\<Definitions>$,
  then in the last example, after popping~$\<Labels>$,~$\<Constants>$
  and~$\<Nested>$, nonterminal~$\<Parameters>$ would also be popped
  for similar reasons, and~$k^*$ would have been~$4$ in this case.

Again, the problem relies with the multiple pops that occur without reading the input.

\subsection{LL(1) parser generator}
\label{section:generation}
The LL(1) parser is based on a prediction table.
For a given nonterminal as the top of the stack, and an input symbol,
  the prediction table provides the next rule to be parsed.
The parser generator fills prediction table, leaving the
  rest to the parsing algorithm in~\cref{algorithm:ll-parser}
The prediction table construction is depicted in~\cref{algorithm:generation}

\begin{algorithm}
  \caption{\label{algorithm:generation}
  An algorithm for filling the prediction table~$\Function predict(A,b)$ for some
    grammar~$G =⟨Σ,Ξ,P⟩$ and an end-of-input symbol~$\$∉Σ$,
    and where~$A∈Ξ$ and~$b∈Σ∪❴\$❵$.}
    \begin{algorithmic}
    \FOR[for each grammar rule]{$r∈P$}
      \LET{$A ::=α$}{$r$}\COMMENT{break~$r$ into its RHS ＆ LHS}
      \FOR[for each terminal in~$\Function First(α)$]{$t∈\Function First(α)$}
        \IF[is there a conflict?]{$\Function predict(A,t)≠⊥$}
          \STATE{\textsc{Error}}\COMMENT{grammar is not LL(1)}
        \FI
        \STATE{$\Function predict(A,t) = r$} \COMMENT{set prediction to rule~$r$}
      \ENDFOR
      \IF[$α$ might derive to~$ε$]{$\Function Nullable(α)$}
        \FOR[for each terminal in~$\Function Follow(A)$]{$t∈\Function Follow(A)$}
          \IF[is there a conflict?]{$\Function predict(A,t)≠⊥$}
            \STATE{\textsc{Error}}\COMMENT{grammar is not LL(1)}
          \FI
          \STATE{$\Function predict(A,t) = r$} \COMMENT{set prediction to rule~$r$}
        \ENDFOR
      \FI
    \ENDFOR
  \end{algorithmic}
\end{algorithm}


