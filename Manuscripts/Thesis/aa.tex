\section{Fluent APIs Java}
Ever after their inception\urlref{http://martinfowler.com/bliki/FluentInterface.html} \emph{fluent APIs}
  increasingly gain popularity~\cite{Hibernate:06,Freeman:Pryce:06,Larsen:2012} and research
  interest~\cite{Deursen:2000,Kabanov:2008}.
In many ways, fluent APIs are a kind of
  \emph{internal} \emph{\textbf Domain \textbf Specific \textbf Languages}~\cite{VanDeursen:Klint:2000,Hudak:1997,Fowler:2010}:
They make it possible to enrich a host programming language without changing it.
Advantages are many: base language tools (compiler, debugger, IDE, etc.) remain
  applicable, programmers are saved the trouble of learning a new syntax, etc.
However, these advantages come at the cost of expressive power;
  in the words of Fowler:
  ‟\emph{Internal DSLs are limited by the syntax and structure of your base language.}”†
  {M. Fowler, \emph{Language Workbenches: The Killer-App for Domain Specific Languages?},
    2005
    \newline
  \url{http://www.martinfowler.com/articles/languageWorkbench.html}}.
Indeed, in languages such as \CC, fluent APIs
  often make extensive use of operator overloading (examine, e.g., \textsf{Ara-Rat}~\cite{Gil:Lenz:07}),
  but this capability is not available in \Java.

Despite this limitation, fluent APIs in \Java can be rich, expressive,
and in many cases can boost productivity in elegant and neat code snippets such as

\begin{quote}
  \javaInput[minipage,width=\linewidth,left=-2ex]{../Fragments/camel-apache.java.fragment}
\end{quote}

(a use case of Apache Camel~\cite{Ibsen:Anstey:10}, open-source integration
framework), and,

\begin{quote}
  \javaInput[minipage,width=\linewidth,left=-2ex]{../Fragments/jOOQ.java.fragment}
\end{quote}

(a use case of jOOQ\urlref{http://www.jooq.org}, a framework for writing SQL
like code in \Java, much like LINQ project~\cite{Meijer:Beckman:Bierman:06} in
the context of \CSharp).

Other examples of fluent APIs in \Java are abundant:
jMock~\cite{Freeman:Pryce:06},
Hamcrest\urlref{http://hamcrest.org/JavaHamcrest/},
EasyMock\urlref{http://easymock.org/},
jOOR\urlref{https://github.com/jOOQ/jOOR},
jRTF\urlref{https://github.com/ullenboom/jrtf}
and many more.

The actual implementation of these many examples is traditionally not carried
out in the neat manner it could possibly take. Our reason for saying this is
that the fundamental problem in fluent API design is the decision on the
‟language”. This language is the precise definition of which sequences of
method applications are legal and which are not.

As it turns out, the question of whether a BNF definition of such a language
can be ‟compiled” automatically into a fluent API implementation is, a
question of the computational power of the underlying language. In \Java, the problem
is particularly interesting since in \Java (unlike e.g., \CC~\cite{Gutterman:2003}),
the type system is (probably) not Turing complete.

The question is then, what can be computed, and what can not be computed by
coercing the type system and the type checker of a certain programming language
to do abstract computations it was never meant to carry out? And, why should we
care?

The previous jOOQ example suggests that jOOQ imitates SQL, but, is it possible
at all to produce a fluent API for the entire SQL language, or XPath, HTML,
regular expressions, BNFs, EBNFs, etc.?

Of course, with no operator overloading it is impossible to fully emulate
tokens; method names though make a good substitute for tokens, as done in the
Apache Camel code excerpt above

\begin{quote}
  \javaInput[minipage,width=45ex,left=-2ex]{../Fragments/jOOQ-mini.java.fragment}.
\end{quote}

The questions that motivate this research are:
\begin{quote}
  \begin{itemize}
    \item Given a BNF specification of a DSL, determine whether there exists a
      fluent API in \Java that can be made for this specification?

    \item In the cases that such fluent API is possible, can it be produced
      automatically?

    \item Is it feasible to produce a \emph{compiler-compiler} such as
      Bison~\cite{Bison:manual} to convert such language specification into a
      fluent API?

\end{itemize}
\end{quote}

Inspired by the theory of formal languages and automata,
  this study explores what can be done with fluent APIs in \Java.

\section{A Brief Review of Fajita - our Main Goal}
\label{section:goal}

Consider \Cref{figure:sql-bnf}, a BNF specification of a fluent API for a certain
fragment of SQL.

\begin{figure}[H]
  \caption{\label{figure:sql-bnf}
    A BNF for a fragment of SQL select queries.
  }
  \begin{Grammar}
    \begin{aligned}
      \<Query> & \Derives \cc{select()} \<Quant>\~\cc{from(Table.\kk{class}\!\!\!)} \<Where> \hfill⏎
      \<Quant> & \Derives \cc{all()} \hfill⏎
                  & \| \cc{columns(Column[].\kk{class}\!\!\!)} \hfill⏎
      \<Where> & \Derives \cc{where()} \cc{column(Column.\kk{class}\!\!\!)} \<Operator> \hfill⏎
                  & \|ε \hfill⏎
      \<Operator> & \Derives \cc{equals(Expr.\kk{class}\!\!\!)}\hfill⏎
                  & \| \cc{greaterThan(Expr.\kk{class}\!\!\!)} \hfill⏎
                  & \|\cc{lowerThan(Expr.\kk{class}\!\!\!)} \hfill
    \end{aligned}
  \end{Grammar}
\end{figure}

To create a \Java implementation that realizes this fluent API,
  the designer feeds the grammar to \Fajita, as in
  \cref{figure:sql-bnf-java}.

\begin{figure}[H]
  \caption[Defining BNFs using \Fajita]{\label{figure:sql-bnf-java}
    A \Java code excerpt defining the BNF specification of the fragment SQL
    language defined in \cref{figure:sql-bnf}.}
  \javaInput[minipage,width=\linewidth,left=-6ex]{sql.bnf.listing}
\end{figure}

We see that \Fajita's API is fluent in itself, and the
  call chain in \cref{figure:sql-bnf-java}, is structured almost
  exactly as in derivation rules in \cref{figure:sql-bnf}.
In particular, the code in \cref{figure:sql-bnf-java} shows how fluent API specification in \Fajita
  may include parameterless methods (\cc{select()}, \cc{all()} and \cc{where}) as well as methods which
  take parameters, e.g., method \cc{column} taking parameter of type \cc{Column} and
    method \cc{from} taking a \cc{Table} parameter.

Other than the derivation rules, \Fajita needs to be told the start rule 
  and the sets of terminals and nonterminals.
The start symbol is given in the \cc{start} method that receives a nonterminal,
and the symbol sets are specified in the first method call in the chain where,
  the enumerate types \cc{SQLTerminals} and \cc{SQLNonTerminals} are:

\begin{quote}
  \javaInput[minipage,width=\linewidth,left=-4ex]{sql.enums.listing}
\end{quote}

The call \cc{.go()}, occurring last in the chain, makes \Fajita generate types
and methods realizing the fluent API, in such a way that legal use of the API
like in \cref{figure:sql:legal} is syntactically legal,

\begin{figure}[H]
  \caption[Legal sequences of calls in the sql fragment example]{\label{figure:sql:legal}
  Legal sequences of calls in the sql fragment example,
  where \cc{t} is of type \cc{Table}, \cc{c} is of type \cc{Column} and \cc{e}
  is of type \cc{Expr}}
\javaInput[minipage,width=\linewidth,left=-4ex]{sql.legal.listing}

\end{figure}

while snippets disobeying the BNF specification in \cref{figure:sql-bnf} like
\cref{figure:sql:illegal} , do not type check.

\begin{figure}[H]
  \caption[Illegal sequences of calls in the sql fragment example]{\label{figure:sql:illegal}
  Illegal sequences of calls in the sql fragment example,
  where \cc{t} is of type \cc{Table} and \cc{c} is of type \cc{Column}.}
  \javaInput[minipage,width=\linewidth,left=-4ex]{sql.illegal.listing}
\end{figure}

\section{The many trials of doing it in Java}
Fluent APIs are neat, but design is complicated,
involving theory of automata, type theory, language design, etc.

And still, automatic generation of these exist to some extent.
One example, is fluflu\urlref{https://github.com/verhas/fluflu}: a software artifact that uses
\Java annotations to define deterministic finite automata (henceforth, DFA), and then
compiles it to a fluent API based on this automaton. Use example of fluflu is
depicted at \cref{figure:fluflu}.

\begin{figure}[ht]
  \caption{\label{figure:fluflu}
    Use example of fluflu, generation a fluent
  API for the regular expression~\texorpdfstring{$a^*$}{a*}}
  \javaInput[minipage,left=-4ex]{fluflu.example.listing}
\end{figure}
The code excerpt in~\cref{figure:fluflu} defines a single stated DFA using
fluflu. The state is both initial and accepting, with a single self
transition, labeled with terminal~$a$. The regular language realized by the
automaton is~$L=a^*$.

Although fluent API generation was ‟achieved”, this example has many flaws, some of them are:

\begin{enumerate}
  \item Using fluflu is complicated and the resulted code is messy.
  \item Defining a DFA is harder then writing, for example, the equivalent regular
        expression (and in some cases, the size of the DFA is exponentially bigger)
  \item Languages defined by DFA can only be regular, a rather small
        class of languages.
\end{enumerate}

A question rises after seeing \Fajita and fluflu: how come the representation
of simple languages such as regular languages is so complex (as seen in
\cref{figure:fluflu}) and the representation of a more complex class of
languages, is rather simple (as seen \cref{figure:sql-bnf-java}?) As it turns
out, two factors are involved in the answer.

The first is the complexity of \emph{representing the language}.
Regular language are mostly defined by regular expression, but the language of
all regular expressions is context-free and not regular
(Intuitively, since regular expressions uses parenthesizing). On the contrary,
the definition of context-free languages is usually done with BNFs (or 
context-free grammars), and the language of all BNFs is regular.

Thus, in order to define a fluent API for generating regular languages, one needs a
fluent API defined by a context-free language, while in order to define a fluent API for
context-free languages, only a regular fluent API is needed.

Since fluflu ‟knows” only how to generate fluent APIs for regular languages,
it cannot represent a fluent API for the generation of itself.
Respectively, since \Fajita's language is regular, and can generate context-free
  fluent APIs, \Fajita can generate itself.

The second factor is the \emph{practical complication} of generating the \Java types to
support the fluent API. As covered in \cref{section:example} the generation of
fluent APIs for regular languages is rather simple, while the generation of
those for context-free languages is the main challenge of this thesis.

\section{Contribution}
\label{section:contribution}
The main results of this research are:
\begin{quote}
  \begin{enumerate}
  \item 
    If the DSL specification is that of a deterministic context-free language,
    then a fluent API exists for the language, but we do not know whether such
    a fluent API exists for more general languages.
    \par
    Recall that there are universal cubic time parsing
    algorithms~\cite{Cocke:1969,Earley:1970,Younger:1967} which can parse (and
    recognize) any (non-deterministic) context-free language. What we do not
    know is whether algorithms of this sort can be encoded within the framework
    of the \Java type system.
  \item
    There exists an algorithm to generate a fluent API that realizes any
    deterministic context-free languages. Moreover, this fluent API can create
    at runtime, a parse tree for the given language. This parse tree can then
    be supplied as input to the library that implements the language's
    semantics.
  \item
    Unfortunately, a general purpose compiler-compiler is not yet feasible with
    the current algorithm.
    \begin{itemize}
      \item 
        One difficulty is usual in the fields of formal languages: The
        algorithm is complicated and relies on modules implementing complicated
        theoretical results, which, to the best of our knowledge, have never
        been implemented.
      \item
        Another difficulty is that a certain design decision in the
        implementation of the standard \texttt{javac} compiler is likely to
        make it choke on the \Java code generated by the algorithm.
    \end{itemize}
  \item
    We did implement a prototype for a compiler-compiler that works for LL(1)
    grammars.
  \end{enumerate}
\end{quote}

Other concrete contributions made by this work include
\begin{itemize}
  \item the understanding that the definition of fluent APIs is analogous to
      the definition of a formal language.
  \item a lower bound (deterministic pushdown automata)
    on the theoretical ‟computational complexity” of the \Java type system.
  \item an algorithm for producing a fluent API for deterministic context-free languages (even if impractical).
  \item a collection of generic programming techniques, developed towards this algorithm.
  \item a demonstration that the runtime of Oracle's \texttt{javac} compiler may be exponential in the program size.
  \item a new interpretation to the LL parsing algorithm, under a ‟realtime” constraint.
\end{itemize}

The theoretical result that any deterministic context-free grammar can be
automatically ‟compiled” to fluent API takes a toll of exponential blowup.
Specifically, the construction first builds a deterministic pushdown automaton
whose size parameters,~$g$ (number of stack symbols), and,~$q$ (number of
internal states), are polynomial in the size of the input grammar. This
automaton is then emulated by a weaker automaton, with as many as
\[
  O\left(g^{O(1)}\left(q²g^{O(1)}\right)^{qg^{O(1)}}\right)
\]
stack symbols.
This weaker automaton is then ‟compiled” into a collection of generic \Java types,
where there is at least one type for each of these symbols.

This work present an algorithm to compile an LL grammar of a fluent API
language into a \Java implementation whose size parameters are linear in
the size parameters of the LL parser generated by the classical
algorithm (\cref{algorithm:generation}) for computing LL parsers,
i.e., the performance loss due to implementation within the \Java
type checker is as small as we can hope it to be.

The savings are made possible by the use of a stronger automaton (the \RLLp,
described in detail below in \cref{section:realtime}) for the emulation, and
more efficient ‟compilation” of the \RLLp into the \Java type system.
We also present \Fajita
†{\itshape \textbf Fluent \textbf API for \textbf J\textsc{ava}
  (\textbf Inspired by the \textbf Theory of \textbf Automata)
}
a \Java tool that implements this algorithm.

\section{Related Work}

It has long been known
  that \CC templates are Turing complete in the following precise sense:

\begin{Proposition}
  \label{Theorem:Gutterman}
  For every Turing machine,~$m$, there exists a \CC program,~$Cₘ$ such that
    compilation of~$Cₘ$ terminates if and only if
      Turing-machine~$m$ halts.
      Furthermore, program~$Cₘ$ can be effectively generated from~$m$~\cite{Gutterman:2003}.
\end{Proposition}

Intuitively, this is due to the fact that templates in \CC
  feature both recursive invocation and conditionals (in the form of
  ‟\emph{template specialization}”).

In the same fashion, it should be mundane to make the judgment that
  \Java's generics are not Turing-complete since they offer no conditionals.
Still, even though there are time complexity results regarding type systems in functional
  languages, we failed to find similar claims for \Java.

Specialization, conditionals, \kk{typedef}s and other features of \CC templates,
  gave rise to many advancements in template/generic/generative programming
  in the language~\cite{Austern:98,Musser:Stepanov:1989,Backhouse:Jansson:1999,Dehnert:Stepanov:2000},
  including e.g., applications in numeric libraries~\cite{Veldhuizen:95,Vandevoorde:Josuttis:02},
  symbolic derivation~\cite{Gil:Gutterman:98}
  and a full blown template library~\cite{Abrahams:Gurtovoy:04}.

Garcia et al.~\cite{Garcia:Jarvi:Lumsdaine:Siek:Willcock:03} compared
  the expressive power of generics in half a dozen major programming languages.
  In several ways, the \Java approach~\cite{Bracha:Odersky:Stoutamire:Wadler:98}
  did not rank as well as others.

Unlike \CC~\cite{Austern:98,Musser:Stepanov:1989, Backhouse:Jansson:1999,
Dehnert:Stepanov:2000,Gil:Gutterman:98,Abrahams:Gurtovoy:04}, the literature on
meta-programming with \Java generics is minimal, research concentrating on
other means for enriching the language, most importantly
annotations~\cite{Papi:08}.

The work on SugarJ~\cite{Erdweg:2011} is only one of many other attempts
  to achieve the embedded DSL effect of fluent APIs by language extensions.

Suggestions for semi-automatic generation can be found in the work of
Bodden~\cite{Bodden:14} and on numerous locations in the web. None of these
materialized into an algorithm or analysis of complexity. However, there is a
software artifact (the previously reviewed
fluflu\urlref{https://github.com/verhas/fluflu}) that automatically generates a
fluent API that obeys the transitions of a given finite automaton.
