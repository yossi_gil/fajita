public class $ { 
  static interface Cons<Car,Cdr>{ 
    Cons<
      Cons<Car,Cdr>,
      Cons<Car,Cdr>
    > d(); 
}
