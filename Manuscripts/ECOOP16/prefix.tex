\begin{theorem}\label{Theorem:Gil-Levy:2}
  Let~$A$ be a DPDA recognizing a language~$L⊆Σ^*$.
  Then, there exists a \Java type definition,~$J_A$ for types~\cc{L},~\cc{A},~\cc{C} and
    other types such that the \Java command
  \begin{equation}
    \label{Equation:prefix:result}
    \cc{C c = A.build.$\textsf{java}(α)$;}
  \end{equation}
    type checks against~$J_A$ if an only if there exists~$β∈Σ^*$ such
    that~$αβ∈L$ and type \cc{C} is the configuration of~$A$ after reading~$α$.
  Furthermore, for any such~$β$,~\cref{Theorem:Gil-Levy} applies such that the
  \Java command
  \begin{equation}
    \cc{L~$ℓ$ = A.build.$\textsf{java}(αβ)$}\cc{.\$();}
  \end{equation}
    always type-checks.
  Finally, the program~$J_A$ can be effectively generated from~$A$.
\end{theorem}

Informally, a call chain type-checks if and only if it is a prefix
  of some legal sequence.
Alternatively, a call chain won't type-check if there is no
  continuation that leads to a legal string in~$L$.

The proof resembles~\cref{Theorem:Gil-Levy}'s proof.
We provide a similar implementation for a jump-stack
  †{recall that the two formal constructs are of the same expressiveness},
  that will not compile under illegal prefixes.

The main difference between the two theorems is:
  in~\cref{Theorem:Gil-Levy} we allowed illegal call chains to compile,
  but not return the required~\cc{L} type, while in~\cref{Theorem:Gil-Levy:2}
  the illegal chain won't compile at all.

Since the code suggested by the proof highly resembles the previously
  suggested code, mainly the differences will be discussed.

We will use the same running example, defined by~\cref{Table:A}.

\subsection{Main Types}
The main types here are a subset of the previously defined main types.

\begin{quote}
  \javaInput[minipage,width=\ReplaceInThesis{45ex}{\linewidth},left=-2ex]{prefix-proof.configuration.listing}
\end{quote}

First, type \cc{$ΣΣ$} is removed.
A call chain that doesn't represent a valid prefix won't compile,
  thus, there is no need for an error return type such as \cc{$\SigmaΣ$}.
Second, \kk{interface}~\cc{C} is removed.
Without it, the configuration types won't have the
  methods \cc{$σ$1()}, … ,\cc{$σ${}$k$()} and \cc{\$()} from the supertype.
These inherited methods, is what differentiates the previous proof from the current.
Classes \cc{¤} and \cc{E} are defined similarly, except now they don't extend any type.

\subsection{Top-of-Stack Types}
Types \cc{C$γ$1}, … ,\cc{C$γ${}$k$}, still represent stacks
  with \cc{$γ$1}, … ,\cc{$γ${}$k$} as their top element,
  this time, the methods are defined ad-hock, in each type
  (they are not added in this figure as they are added with the use of sidekicks).
In~$A$ there are two such types:

\begin{quote}
  \javaInput[minipage,width=\ReplaceInThesis{51ex}{\linewidth},left=-2ex]{prefix-proof.many.listing}
\end{quote}


Note, that the type parameters of the former types hasn't changed,
  since the model we are trying to implement, hasn't changed.
  These~$k+1$ parameters still suffice for our cause.

\ReplaceInThesis{%
\begin{wrapfigure}[13]r{42ex}
  \caption{\label{Figure:prefix-chain} Accepting and non-accepting call chains with the
  type encoding of jDPDA~$A$ (as defined in \cref{Table:A}).
  All lines in \cc{accepts} type-check, and all lines in \cc{rejects}
  cause type errors}
  \javaInput[minipage,width=42ex,left=-2ex]{prefix-proof.cases.listing}
\end{wrapfigure}
}{%
\begin{figure}[ht]
  \caption{\label{Figure:prefix-chain} Accepting and non-accepting call chains with the
  type encoding of jDPDA~$A$ (as defined in \cref{Table:A}).
  All lines in \cc{accepts} type-check, and all lines in \cc{rejects}
  cause type errors}
  \javaInput[minipage,width=\linewidth,left=-2ex]{prefix-proof.cases.listing}
\end{figure}
}
In~\cref{Figure:prefix-chain}, call chains in the~\cc{accepts()} method
  correctly type-checks (i.e., in~$L$), while the chains in~\cc{rejects()}
  do not type-check (i.e., these prefixes have no continuation that can lead to a legal word in~$L$),
  where the last method invocation generates an
\begin{quote}
  ‟\textsf{method~…~is undefined for the type~…~}”
\end{quote}
  error message.

The main difference between~\cref{Figure:prefix-chain} and~\cref{Figure:chain} is that there is no need to
  use an auxiliary function \cc{isL()} as in~\cref{Figure:prefix-chain} since now illegal
  prefixes do not type-check.

\subsection{Transitions}
Due to the changes we expressed, the transition table is encoded slightly different.

Encoding of the legal operations \textsf{accept},~$\textsf{jump}(γᵢ)$ and~$\textsf{push}(ζ)$
  remains as in~\cref{Theorem:Gil-Levy}, since we want the same behavior for legal call chains.
The minor differences are in the illegal operations \textsf{reject} and~$⊥$:

\begin{description}
 \item[\textsf{reject}] Since we add the methods ad-hock to each type, the reject entry means
   that the corresponding type, \emph{won't} have a~\cc{\$()} method, i.e., type~\cc{C$γ$2}
   doesn't have a method~\cc{\$()}.
 \item[$⊥$] We encounter~$⊥$ on the transition function when some input character~\cc{$σ$}
   is not allowed for the top of the stack element~$γ$. In that case, the corresponding type \cc{C$γ$}
   \emph{must not} have a method for~\cc{$σ$}, this way, invoking the methods will result in type error.
   In \cref{Table:A} a~$⊥$ may occur when the top of the stack is~$γ₁$ and the input character is~$σ₂$,
   thus, no method \cc{$σ$2} is introduced in type~\cc{C$γ$1}.
\end{description}

The use of sidekicks is still allowed and recommended to improve readability of code.

\begin{figure}[ht]
  \caption{\label{Figure:prefix-A}Type encoding of jDPDA~$A$ (as defined in \cref{Table:A})
    that allow a partial call chain, if and only if, there exists a legal continuation, that
    leads to a word in~$L$ (the language of~$A$)}
  \javaInput[minipage,listing style=numbered,width=1.08\textwidth]{prefix-proof.full.listing}
\end{figure}

\paragraph*{Conclusion}
In this section, a proof, similar to the one in~\cref{section:proof} is provided.
An algorithm was introduced, to not only emulate the running of some jDPDA~$A$,
  but also to ‟halt it” in the earliest time possible, i.e., only if there is
  no legal call chain from this point to result in a legal word in the language of~$A$.
