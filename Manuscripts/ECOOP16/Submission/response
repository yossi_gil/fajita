No matter which way we turn the meticulous reviews, we are compelled to admit:
The reviews hit the nail right on the head. The current version of the paper
simply did not do a good job in presenting the motivation and the context of
this work.

The revised paper shall present this crucial portion as in this sketch:

The elegance, utility, and the increasing rate at which fluent APIs are
introduced [citations] lead to  the natural question of automatic production of
these.  This research begun with the naive, first look impression that the
problem is easy, and it is only a matter of skilled engineering work to
automate the process.  Deep into this work, we discovered how recalcitrant the
problem is.  We were repeatedly mislead to believe that the engineering task
could be solved by employing a slightly heavier artillery of generic
programming.  A literature survey shows that the same failure was experienced
by others. There are several systems [citations] for generating fluent APIs,
but none of them solves the problem completely.

Expressing fluent API design in terms of automata and formal languages made it
possible to identify the main hurdle ($\epsilon$-moves) and the means for
overcoming it (jump-stacks).  Indeed, the theoretical result does not directly
solve the practical problem, but it gives hints on how this might be done.

The historical perspective shows that the theory of formal languages, BNFs,
context free grammars, and automata lead to concrete, top-down and bottom-up,
parsing algorithms.  We trust that the same is expected here.  In fact, we now
work on developing concrete, efficient algorithms for recognizing and parsing
with Java generics, based on the deeper understanding we gained from the
theoretical results.

Current results are mixed: We can show that parsing is impossible while
relying solely on generic programming, but can be implemented in a manner
mentioned in the current paper after recognition.  On the other hand, we are on
the verge of a recognition algorithm which is the amalgam of Knuth's classical
LR parsing and the jump-stack model.  When (and if) these results mature, they
will not be part of this paper.  But, the mention of these is to explain the
practical relevance of the theoretical results.

Answers to specific questions and brief response to some comments:

Reviewer A: 

* The 77 paper does not address the size of the reduction.  The revised paper
will show the blowup due to the reduction is exponential in
the worst case. We believe that this is not the case in "typical" language.

* Indeed! A research paper is not a thriller, and we ought
to include "spoilers" to prepare the reader for advancements in the plot,
rather than shock him with twists.

Q1: We rely only on generics and raw types. No Java 8 features.

Q2: To the contrary. We are forced to use JDPDA rather than DPDA since DPDAs
seem to be impossible with Java.


Reviewer B:

* Sure, we will discuss LINQ in the revised paper.

* We argue that the results apply to a "deep" fluent API,
not only to shallow ones. The revised paper will highlight this point and
explain the difference between the two sorts.

B1: We investigated wildcards, intersection types and (the rather limited) type
inference of Java, but failed to employ these to increase the expressive power
of the computation model.

B2: We do not understand the question. But, the revised paper will include a
corollary re: RE. 

Reviewer D:

* The proof will be made formal.

* With the above explanation, the scent may become a smell, and perhaps even a
taste.

D1: Thanks for the question! See blurb above. 

D2: Double the thanks for this one!!  We are excited by the idea of comparing
manual construction with automatic generation, and plan to include a discussion
of LINQ.

PS. It goes without saying that typos, glitches and bugs  indicated by the
reviewers and others we have noticed will be exterminated, with the silent
prayer that not too many new ones are introduced in the process. 

Finally, we enjoyed reading the clever reviews. Thanks!



