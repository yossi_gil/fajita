The pattern ‟invoke function on variable \cc{sb}”, specifically with
  a function named \cc{append}, occurs six times in the code in \cref{Figure:chaining}(a), designed
  to format a clock reading, given as integers hours, minutes and
  seconds.

\begin{figure}[H]
  \caption[Example of recurring method invocations]{\label{Figure:chaining}%
    Recurring invocations of the pattern ‟invoke function on the same
      receiver”, before, and after method chaining.
  }%
    \begin{tabular}{@{}cc@{}}%
  \begin{lcode}[minipage,width=44ex,box align=center]{Java}
String time(int hours, int minutes, int seconds) {¢¢
  final StringBuilder sb = new StringBuilder();
  sb.append(hours);
  sb.append(':');
  sb.append(minutes);
  sb.append(':');
  sb.append(seconds);
  return sb.toString();
}\end{lcode}
\hfill
&
\hspace{1ex}
  \begin{lcode}[minipage,width=44ex,box align=center]{Java}
String time(int hours, int minutes, int seconds) {¢¢
    return new StringBuilder()
      ¢¢.append(hours).append(':')
      ¢¢.append(minutes).append(':')
      ¢¢.append(seconds)
      ¢¢.toString();
}\end{lcode}
⏎
\textbf{(a)} before & \textbf{(b)} after
\end{tabular}
\end{figure}

Some languages, e.g., \Smalltalk offer syntactic sugar, called \emph{cascading},
  for abbreviating this pattern.
\emph{Method chaining} is a ‟programmer made” syntactic sugar serving the same purpose:
  If a method~$f$ returns its receiver, i.e., \kk{this},
  then, instead of the series of two commands: \mbox{\cc{o.$f$(); o.$g$();}}, clients can write
  only one: \mbox{\cc{o.$f$().$g$();}}.
  \cref{Figure:chaining}(b) is the method chaining
  (also, shorter and arguably clearer) version of
  \cref{Figure:chaining}(a).
It is made possible thanks to the designer of class \cc{StringBuilder} ensuring that 
  all overloaded variants of
  \cc{append} return their receiver.

The distinction between \emph{fluent API} and method chaining is the identity of the receiver:
In method chaining, all methods are invoked on the same object, whereas in fluent API
the receiver of each method in the chain may be arbitrary.
Fluent APIs are more interesting for this reason.
Consider, e.g., the following \Java code fragment (drawn from JMock~\cite{Freeman:Pryce:06})
\[
  \cc{allowing(any(Object.\kk{class})).method("get.*").withNoArguments();}
\]
Let the return type of function \cc{allowing} (respectively \cc{method}) be denoted by~$τ₁$
(respectively~$τ₂$).
Then, the fact that~$τ₁≠τ₂$ means that the set of methods that can be placed after the dot
in the partial call chain~$\cc{allowing(any(Object.\kk{class})).}$
is not necessarily the same set of methods that can be placed after the 
dot in the partial call chain \[
\cc{allowing(any(Object.\kk{class})).method("get.*").}.
\]
This distinction makes it possible to design expressive and rich fluent APIs, in which a
sequence of ‟chained” calls is not only readable, but also robust, in the sense that the
sequence is type correct only when it makes sense semantically.

There is a large body of research on \emph{type-states} 
(See e.g., review articles such
  as~\cite{Aldrich:Sunshine:2009,Bierhoff:Aldrich:2005}).
Informally, an object that belongs to a certain type, has
type-states, if not all methods defined in this object's class are applicable
to the object in all states it may be in.
As it turns out, objects with type states are quite frequent: a recent study~\cite{Beckman:11} estimates
  that about 7.2％ of \Java classes define protocols, that can be interpreted as type-state.

In a sense, type states define the ``language'' of the protocol of an object. 
The protocol of the type-state \cc{Box} class defined in \cref{Figure:box} 
  admits the chain \cc{\kk{new} Box().open().close()} but not
  the chain \cc{\kk{new} Box().open().open()}.

\begin{figure}[H]
  \caption{\label{Figure:box}Fluent API of a box object, defined by a DFA and a table}
  \begin{tabular}{cc}
    \hspace{7ex}\parbox[c]{40ex}{%
      \begin{tabular}[align=center]{m{7ex} | m{9ex} @{}| m{9ex}}
        & \cc{open()} & \cc{close()}⏎ \hline
        ‟closed”\ & \color{blue}{\emph{become ‟open”}} & \color{red}{\emph{runtime error}}⏎\hline
        ‟open” & \color{red}{\emph{runtime error}} & \color{blue}{\emph{become ‟closed”}}⏎
      \end{tabular}
    } &
    \hspace{-1ex}\parbox[c]{40ex}{\input{../Figures/open-close-example.tikz}}
    ⏎⏎
    \hspace{0ex}\textbf{(a)} Definition by table & \hspace{-2ex}\textbf{(b)} Definition by DFA
  \end{tabular}
\end{figure}

As mentioned above, tools such as fluflu realize
  type-state based on their finite automaton description.
Our approach is a bit more expressive: examine the language $L$ defined by the type-state, 
  e.g., in the box example,  
        \[
          L = \big(\cc{.open().close()}\big)^*\big(\cc{.open()}\:|\:ε\big).
        \]
If $L$ is deterministic context-free, a fluent API can be made for it. 

To make the proof concrete, consider this example of fluent API definition:
An instance of class \cc{Box} may receive two 
  method invocations: \cc{open()} and \cc{close()}, and can be in either 
  ‟open” or ‟closed” state.
Initially the instance is ‟closed”.
Its behavior henceforth is defined by \cref{Figure:box}.

To realize this definition, we need a type definition by which \cc{\kk{new} Box().open().close()}, more generally
  blue, or accepting states in the figure, type-check.
Conversely, with this type definition, compile time type error should occur in \cc{\kk{new} Box().close()},
  and, more generally, in the red state.

Some skill is required to make this type definition: proper design of class \cc{Box}, perhaps with
  some auxiliary classes extending it, an appropriate method definition here and there, etc.
