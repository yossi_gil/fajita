\def\function#1(#2){\ensuremath{\textsf{#1}(#2)}}
\def\table#1[#2]{\ensuremath{\textsf{#1}[#2]}}

The JLR recognizer is a shift-jump machine
  based on the jDPDA automaton.
It was invented with inspiration from the classical
  LR(1) parser, with changes regarding the automaton's
  \emph{realtime} nature.

The core of LR parsing are the LR items,
  which construct the LR automaton's states, and
  characterize the shift and reduce operations.
The following definition of JLR items,
  encloses the changes required to those items to handle
  jump operations.
A JLR item is a tuple~$⟨i,ℓ⟩$ where~$i$ is an
  LR item, and~$ℓ$ is a special stack element,
  to which the recognizer will jump, in case this item will
  be reduced.
This jump is designed to emulate the LR's
  reduce operation, considering the unbounded list of consecutive
  reductions that might occur.
We denote such items as~$[A→α·β, b ,ℓ]$
  where~$A→α·β, b$ is the original LR notation, and~$ℓ$
  is the jump address.

A set of JLR items is said to be a JLR state.

\subsection{States generation algorithm}
The method for constructing a shift-jump automaton
  is basically the same as the LR automaton construction,
  with small modification to update the addresses of JLR items
  and modifications regarding nullable symbols.
Like the original algorithms, we divide the work to three
  sub-routines~$\function Closure(I)$,~$\function Goto(I,X)$ and~$\function Items(G')$,
  where the first slightly differs from the original function by handling the
  addresses of freshly added items as well as nullable symbols,
  and the other two remain as in the original LR state construction.
The algorithms are depicted in~\cref{Algorithm:Closure},~\cref{Algorithm:Goto}
  and~\cref{Algorithm:Items} accordingly.

\newcommand\INPUT\REQUIRE
\newcommand\OUTPUT\ENSURE
\renewcommand{\LET}[2]{\STATE{\textbf{Let} \ensuremath{\text{#1}←\text{#2}}}}
\renewcommand{\algorithmicrequire}{\textbf{Input}}
\renewcommand{\algorithmicensure}{\textbf{Output}}

\begin{algorithm}[p]
    \newcommand\Queue{\ensuremath{Q}}
  \caption{\label{Algorithm:Closure}
  Function~$\function Closure(I)$: compute the closure of items set~$I$}
  \begin{algorithmic}[1]
    \INPUT{a set of JLR items~$I$}
    \OUTPUT{$J$, the set of JLR items obtained by a closure of~$I$}
    \STATE{$\Queue←I$ \COMMENT{a queue used for breadth-first search}}
    \STATE{$J←∅$ \COMMENT{initial approximation of returned value}}
    \WHILE[more work to do]{$\Queue ≠ ∅$}
    \LET{$i$}{$\function dequeue(\Queue)$} \COMMENT{consider next JLR $i$}
    \STATE{$J←J ∪ ❴i❵$} \COMMENT{add $i$ to result before visiting its derivatives} 
      \STATE{\textbf{Let}~$[A→α·β, a,ℓ]←i$}
      \COMMENT{break item~$i$ into its~$A$,~$α$,~$β$,~$a$, and~$ℓ$ components}
      \IF[dot is not before a nonterminal] {$β=ε ∨ β=σβ', σ∈Σ$}
        \STATE{\textbf{continue}} \COMMENT{$i$ does not have any derivatives}
      \ENDIF
      \LET{$Bδ$}{$β$}\COMMENT{break $β$ into nonterminal $B$ and sequence $δ$}
      \IF[add to closure item representing derivation~$B\overset*⇒ε$]{$\function Nullable(B)$}
        \STATE{$\function enqueue(\Queue,[A→αB·δ,a,ℓ])$}
        \COMMENT{if $B$ is nullable, state includes item for $B \overset*⇒ε$ }
      \ENDIF
      \FOR[add to closure whatever~$Bδ$ may derive]{$B→γ∈G'$,$γ≠ε$}
      \IF[add to closure item representing~$δ\overset*⇒ε$]{$\function Nullable(δ)$}
          \STATE{$\function enqueue(\Queue,[B→·γ,a,ℓ])$}
          \COMMENT{item for starting derivation rule $B →γ$} 
        \ENDIF
        \FOR[add to closure item representing derivation~$δ\overset*⇒t⋯$]{$t∈\function First(δ)$}
          \STATE{$\function enqueue(\Queue, [B→·γ,t,\function newLabel()])$}
        \ENDFOR
      \ENDFOR
    \ENDWHILE
    \RETURN{$J$}
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}[p]
  \caption{\label{Algorithm:Goto}
  Function~$\function Goto(I,X)$: generate an successive item set for~$I$ after seeing~$X$}
  \begin{algorithmic}
    \INPUT{a set of JLR items~$I$}
    \INPUT{a grammar symbol~$X$}
    \OUTPUT{the successive item set}
    \STATE{$J←∅$} \COMMENT{initially, there are not items}
    \FOR{$i∈I$}
      \LET{$[A→α·Xβ,a,ℓ]$}{$i$}\COMMENT{break item~$i$ into components}
      \LET{$i'$}{$[A→αX·β,a,ℓ]$}\COMMENT{advance the dot}
      \alpha
      \STATE{$J←J ∪ ❴i'❵~$}
    \ENDFOR
    \RETURN{$\function Closure(J)$}
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}[p]
  \caption{\label{Algorithm:Items}
  Function~$\function Items(G')$: generate set of states for
      augmented grammar~$G'$}
  \begin{algorithmic}
    \INPUT{An augmented grammar~$G'$}
    \OUTPUT{the set of JLR states induced by~$G'$}
    \STATE{$R ← ❴[S'→·S, \$, \function newLabel()]❵$}
    \COMMENT{work list and return value}
    \REPEAT[processing the work list]
    \FOR[deal with items in set~$I$]{$I ∈ R$}
  \FOR{each grammar symbol~$X ∈Σ_\$ ∪ Ξ'$}
    \IF{$\function Goto(I,X)$ is not empty and not in~$R$}
    \STATE{$R←R ∪ ❴\function Goto(I,X)❵$}
    \ENDIF
  \ENDFOR
      \ENDFOR
    \UNTIL{Set~$R$ is unchanged}
    \RETURN{$R$}
  \end{algorithmic}
\end{algorithm}

Notes on the algorithms
\begin{itemize}
  \item Each JLR state contains all items that would be generated
    on an equivalent LR parser state
    (intuition: without lines 17--18 in \cref{Algorithm:Closure}
    the algorithms differ only in the address field).
\end{itemize}

\subsection{Structure of the JLR action table and behavior of actions}
The structure of the JLR action table
  resembles the LR(1) action table;
  it is defined by a function~$\table ACTION[q,a]$
  where~$q∈Q$, and~$a∈Σ_\$$, being the current state and
  the lookahead symbol accordingly.
The result of~$\table ACTION[q,a]$ can have one of
  three forms:
\begin{description}
  \item[$\textsf{accept}$] The recognizer accepts on input.

  \item[$s(f,p)$] A shift operation.
  The recognizer pushes~$f∈ℕ↛Q$ and~$p∈Q$ onto the stack
    in that order.

  \item[$j(n)$] A jump operation. The recognizer jumps
    to the address numbered as~$n$,
    Specifically, The recognizer pops elements from the
    stack until some function~$f$ is popped, such that~$f(n)$
    is defined, and yields~$q$.~$q$ is then pushed onto the stack.
\end{description}

As the last step before constructing the action table, we
  define a function~$\textsf{JS}: Q⨉Σ→(ℕ↛Q)$ (jump set).
$\function JS(q,a)$ returns a function~$f$ that maps labels
  to states.
In a way, it reminds the~$Goto$ table used in LR parsing, since
  that function will be used when a jump operation will occur.

\begin{algorithm}[ht]
  \begin{algorithmic}
    \caption{\label{Algorithm:JS}
      auxiliary function for action table}
    \INPUT{current state~$q$}
    \INPUT{lookahead symbol~$a$}
    \OUTPUT{a mapping between labels and states}
    \STATE{$f←❴❵$}
    \FOR{$[ A→α·aβ, b,ℓ]∈\function Kernel(q)$} % Problem: we add to the function only for non-kernel items, now the distinction is a bit elusive.
      \STATE{$ f(\ell) = \function Goto(\function Goto(q,A),b)$}
    \ENDFOR
    \RETURN{$f$}
  \end{algorithmic}
\end{algorithm}

In order to build the action table, we need the set of states generated
  by~\cref{Algorithm:Items}.

\endinput

\paragraph{Misc.}

\subsection{The LR($1$) parser}
An LR($1$) parser (henceforth, LR parser) for a grammar~$G=(Ξ,ξ,R)$
  (Nonterminals set, initial start element, production rules set respectively)
  has three components:
\begin{description}
  \item[An automaton] whose states are~$Q=❴q₀,…,qₙ❵$
  \item[A stack] which may contain any member of~$Q$.
  \item[Specification of state transition] with the aid of two tables:
  \begin{description}
    \item[Goto table] which defines a partial function~$δ:Q⨉Ξ↛Q$ of transitions
    between the states of the automaton.
    \item[Action table] which
    defines a partial function\[η:Q⨉Σ↛ ❴ \textsf{Shift}(q) \,|\, q∈Q❵ ∪ ❴\textsf{Reduce}(r) \,| \, r∈G❵.\]
  \end{description}
\end{description}
An \emph{LR item} is a rule~$r∈R$ and a dot index in the right-hand side of the rule,
  i.e.,~$A→α·β$ is an LR item if~$A∈Ξ$,~$A→αβ∈R$,
  the dot index in this example is~$|α|$.
Every state~$q∈Q$ is a set of~\emph{LR items},
  where every such item is in the~$Kernel_{q}$ set,
  or in the~$NonKernel_{q}$ set.
Every state~$q$ is the unification of~$Kernel_{q}$ and~$NonKernel_{q}$.
The parser begins by pushing~$q₀$ (the initial state) into the empty stack,
and then repetitively executes the following:
Examine~$q∈Q$, the state at the top of the stack.
If~$q=qₙ$ (the accepting state) and the input string is wholly consumed, then the parser stops and the input is accepted.

Let~$σ∈Σ$ be the next input symbol.
If~$δ(q,σ)=⊥$ (undefined), the parser stops in rejecting the input.

If~$δ(q,σ) = \textsf{Shift}(q')$, the parser pushes state~$q'$
into the stack and consumes~$σ$.
If however,~$δ(q,σ) = \textsf{Reduce}(r)$,~$r=ξ→β$,
then the parser does not consume~$σ$.
Instead it makes~$|β|$ pop operations.
Let~$q'$ be the state at the top of the stack after these pops, then
the parser pushes a state,~$q”$,
defined by applying the transition function to~$ξ$, the left-hand side of the reduced production and~$q'$,
i.e.,~$q”=η(q',ξ)$.

The specifics of parser generation and specifically
  calculating the sets~$Kernel_{q}$ and~$NonKernel_{q}$, lies beyond our interest here.
We do mention however that the same LR parser can serve parsing table drawn using weaker LR parser generators,
including SLR and LALR\@.

\subsection{Between LR and JLR - A direct approach}
LR parsers are actually DPDAs\@. As we mentioned before in \cref{section:proof},~$ε$-moves
  fails the emulation of a DPDA, thus, in order to type-encode an LR parser, we first need
  to convert it to the encode-able jDPDA\@.

A \emph{JLR parser}, or \emph{Jump LR Parser} is an LR parser, that
  is based on a variation of the jDPDA\@.
The objective of the JLR parser is refined in~\cref{Theorem:JLR}.

\begin{Theorem}
  \label{Theorem:JLR}
  Let~$P$ be an LR parser, then there exists a jDPDA~$J$,
  such that for every string~$α∈Σ^*$, parser~$P$ accepts~$α$
    if and only if~$J$ accepts~$α$
\end{Theorem}

For an LR parser~$⟨Q,δ_{LR}, η_{LR}⟩$ defined over a grammar~$⟨Ξ,ξ,R⟩$
A JLR is like the jDPDA~$A=⟨Γ,q₀,δ⟩$ defined in~\cref{Definition:JDPDA},
  where~$Γ= Q∪(Σ_\$↛Q)$ are the stack elements,~$q₀∈Q$
  is the initial state, and~$delta:Γ⨉Σ↛Γ^*∪j(Σ_\$)$ is defined as follows:
  \begin{itemize}
   \item~$δ(q,σ)= j(σ)$ if~$η_{LR}(q,σ)=Reduce(A→α)∧α≠ε$

   % Automization based on the second example (Balanced parenthesis)

   % Green rule , only on kernel items
   \item~$δ(q,σ)= q \: p~$ if~$η_{LR}(q,σ)=Shift(p)$ and we advanced only on~$\textsf{Kernel}_q$ items
   \item~$δ(q,σ)= q \: p~$ if~$η_{LR}(q,σ)=Reduce(A→ε)$
     and~$δ_{LR}(q,A)=Goto(q')$ and~$η_{LR}(q',σ)=Shift(p)$
     and we advanced only on~$\textsf{Kernel}_q$ items

  % Red rule , only on non-kernel items

   \item~$δ(q,σ)= q \: f \: p~$ if~$η_{LR}(q,σ)=Shift(p)$ and we advanced only on~$I⊆\textsf{NonKernel}_q$
     items, then
    \[
      f = ❴σ'↦q' \; | \;∃i∈I. lookahead(i)=σ' \,∧\, η_{LR}(q,σ')=Shift(q')❵
    \]

   \item~$δ(q,σ)= q \: f \: p~$ if~$η_{LR}(q,σ)=Reduce(A→ε)$
     and~$δ_{LR}(q,A)=Goto(q')$ and~$η_{LR}(q',σ)=Shift(p)$
     and we advanced only on~$I⊆\textsf{NonKernel}_q$
     items, then
     \[
      f = ❴σ'↦q' \; | \;∃i∈I. lookahead(i)=σ' \,∧\, η_{LR}(q,σ')=Shift(q')❵
     \]

  \end{itemize}
\subsection{Informally}

For an LR parser~$⟨Q,δ_{LR}, η_{LR}⟩$ defined over a grammar~$⟨Ξ,ξ,R⟩$
A JLR is like the jDPDA~$A=⟨Γ,q₀,δ⟩$ defined in~\cref{Definition:JDPDA},

\begin{itemize}
  \item When we \textsf{reduce()} a jump operation is performed.
    The jump will be for the corresponding look-ahead symbol.
  \item On a shift op:
  if we shift on a~$item∈\textsf{Kernel}$
    then everything as usual.
  Else,~$item∈\textsf{Closure(Kernel)}$
      if the look-ahead of this item will be consumed by a kernel item:
        a new Jump symbol will be inserted on the stack.
      Else:
        the look-ahead will cause a reduction of one of the kernel items,
        in that case, the previous Jump symbol is still relevant.
  \item[Even computation Invariant]
    Let~$L$ be an LR parser,~$J$ is the corresponding JLR, and word~$w =σ₁…σₖ \$∈Σ^*＄~$ (we denote~$wᵢ$ to be~$σᵢ⋯σₖ ＄$).
    if when~$L$ parses~$w$, it reaches a configuration~$(wᵢ,γζ)$,
    then when~$J$ parses~$w$, it reaches the configuration~$(wᵢ,γζ')$ where~$ζ= \textsf{clean}(ζ')$
    and \textsf{clean} is a function that removes all~$c∉Q$ from the input string.
  \item[Jump invariant]
    when jumping on a lookahead symbol~$a∈Σ_\$~$ during the computation of~$J$,
    all consecutive reductions performed on~$L$ from the parallel configuration, are ‟done” by the \textsf{jump} operation.
  \item[Parsing] can always be added to this model, because when we get to an item~$A→α·B,a$, and we shift on
    one of the items~$B→·β,a$, we know that every legal parsing will result in (thats actually not accurate, what happens
    if we shift on another item~$A→·γ,a/b$?)
\end{itemize}
\paragraph{Problem 1} if we shift on more than one items, with different lookaheads,
  and put multiple terminals on the stack, who should we put first? the ones that are
  not first are the problem; if we jump to them, we still got other terminals on the
  top of the stack, out invariant says we always want the top of the stack to be a
  state element.

\paragraph{Solution 1} if save on the stack a single element for the
  ‟multiple shifting items” which will be of type:~$𝓟(Σ)$
  and we transform the jump operation on~$σ∈Σ$to be ‟Remove elements
  from the stack until a~$χ∈𝓟(Σ)$,~$σ∈χ$ is popped”.

\paragraph{Problem 2} When we~$\textsf{Jump}(σ)$ , we consume~$σ$, and remove
  from stack as expected, the problem is, the corresponding lookaheads symbol
  should be ‟consumed” and the shift on~$σ$ need to happen again.

\paragraph{Solution 2} Instead of pushing subsets of~$Σ$ we push partial
  mappings~$Σ_{\$}↛Q$ (where~$Γ=Q∪(Σ_{＄}↛Q)$) jumping is still defined
  on symbols~$σ∈Σ_{\$}$, and when the jump is performed we pop from the stack
  until we popped some~$f∈(Σ↛Q)$ for which~$f(σ)≠⊥$, and then push~$f(σ)$ back onto the stack.

\paragraph{Problem 3} When we have a state \textsf{Base} that has
  two items~$A₁→α₁·β₁,a$ and~$A₂→α₂·β₂,a$ we can't decide which function to put,
  the mapping of~$a$ might be for the consecutive
  transitions~$\textsf{Base}.goto(A₁).shift(a)$ or to~$\textsf{Base}.goto(A₂).shift(a)$.

\paragraph{Failed Solution 3} We cannot solve the problem by changing the function elements from~$Σ_\$↛Q$
  to~$Σ_\$⨉Ξ↛Q$. The reason resides on the identity of the input nonterminal.
  When we build the JLR's transition function, and want to add a function element to the stack, we choose for some~$σ∈Σ$
  a transition to be performed after a goto, to a certain state~$q∈Q$, problem is, there might be more than one such transition
  for that~$σ$, i.e., when for some~$q,q',q”∈Q$, and~$A,B∈Ξ$ ,~$q.goto(A).shift(a)=q'$,~$q.goto(B).shift(a)=q”$.

\paragraph{Problem 4} We add a function element on the stack for lookahead~$a∈Σ$
  when we have an item that will consume~$a$ after some reduction,
  i.e., an item of type~$A→·Baα,c$ for some~$A,B∈Ξ$ and~$c∈σ$,
  and we don't add an item when we have a rule that will reduce
  upon seeing such lookahead~$a$, i.e.,~$A→α·B , a$
  ( not quite sure that these examples are as general as possible. I should verify it).
  What happens if both appear in the same state?

\paragraph{Solution 4} I believe that in \emph{every} such situation,
  we would see a Shift/Reduce conflict, at some state reachable from the discussed state.
