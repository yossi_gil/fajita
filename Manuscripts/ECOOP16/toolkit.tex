This section presents techniques and idioms of type encoding in \Java 
  partly to serve in the proof of \Cref{Theorem:Gil-Levy}, 
  and partly to familiarize the reader with 
  the challenges of type encoding. 

Let~$g:Γ↛Γ$ be a partial function,
  from the finite set~$Γ$ into itself.
We argue that~$g$ can
  be represented using the compile-time mechanism of \Java.
  \cref{Figure:unary-function\ReplaceInThesis{}{-b}} encodes such a partial function for~$Γ=❴γ₁,γ₂❵$, where~$g(γ₁)=γ₂$
  and~$g(γ₂)=⊥$, i.e.,~$g(γ₂)$ is undefined.%
†{Unless otherwise stated,
      all code excerpts here represent full implementations,
      and automatically extracted, omitting headers and footers,
    from \Java programs that compile correctly with a \Java 8 compiler.}

\ReplaceInThesis{%
\begin{figure}[hbt]
  \caption[Type encoding of partial unary function]{\label{Figure:unary-function}%
    Type encoding of partial function~$g:Γ↛Γ$,
    defined by~$Γ=❴γ₁,γ₂❵$,~$g(γ₁)=γ₂$ and~$g(γ₂)=⊥$.
  }
  \begin{tabular}{@{}c@{}c@{}c@{}}
    \hspace{-7ex}
    \parbox[c]{0.26\linewidth}{%
      \input{../Figures/unary-function-classification.tikz}
    }%
    &
    \hspace{-1ex}
    \parbox[c]{0.64\linewidth}{%
      \javaInput[]{gamma.listing}
    }%
    &
    \hspace{-18ex}
    \parbox[c]{0.84\linewidth}{%
      \javaInput[,toprule=3pt,leftrule=3pt,bottomrule=3pt]{gamma-example.listing}
    }%
⏎
\textbf{(a)} type hierarchy & \textbf{(b)} implementation & \hspace{-62ex} \textbf{(c)} use cases
  \end{tabular}
\end{figure}
}{%
\begin{figure}[hbt]
  \caption[Type hierarchy of partial unary function]{\label{Figure:unary-function-a}%
    Type hierarchy of partial function~$g:Γ↛Γ$,
    defined by~$Γ=❴γ₁,γ₂❵$,~$g(γ₁)=γ₂$ and~$g(γ₂)=⊥$.
  }\centering
      \input{../Figures/unary-function-classification.tikz}
\end{figure}
\begin{figure}[hbt]
  \caption[Type encoding of partial unary function]{%
    Type encoding of partial function defined in \cref{Figure:unary-function-a}
  }
  \label{Figure:unary-function-b}%
      \javaInput[minipage,width=\linewidth]{gamma.listing}
\end{figure}
\begin{figure}[hbt]
  \caption[Use cases of partial unary function]{%
    Use cases of partial function defined in \cref{Figure:unary-function-a}
  }
  \label{Figure:unary-function-c}%
      \javaInput[minipage,width=\linewidth]{gamma-example.listing}
\end{figure}
}

The type hierarchy depicted in~\cref{Figure:unary-function\ReplaceInThesis{}{-a}}\ReplaceInThesis{(a)}{} shows five classes:
Abstract class~\cc{Γ}~†{Remember that \Java admits Unicode characters in identifier names} represents the set~$Γ$, final classes~\cc{γ1},~\cc{γ2}
  that extend~\cc{$Γ$}, represent the actual members of the set~$Γ$.
The remaining two classes are private final class~\cc{¤} that stands for an error value,
  and abstract class~\cc{$Γ'$} that denotes the augmented set~$Γ∪❴\text{¤}❵$.
Accordingly, both classes~\cc{¤} and~\cc{$Γ$} extend~\cc{$Γ'$}.†{The use
  of short names, e.g.,~\cc{$Γ$} instead of \cc{$Γ'.Γ$},
    is made possible by an appropriate \kk{import} statement omitted here and henceforth.}

    The full implementation of these classes is provided in~\cref{Figure:unary-function\ReplaceInThesis{}{-b}}\ReplaceInThesis{(b)}{}.
This actual code excerpt should be placed as a nested class of some appropriate host class. Import statements are omitted, here and henceforth for brevity.

The use cases in~\cref{Figure:unary-function\ReplaceInThesis{}{-c}}\ReplaceInThesis{(c)}{} explain better
  what we mean in saying that function~$g$ is encoded in the type system:
  An instance of class~\cc{$γ$1} returns a value of type~\cc{$γ$2} upon
  method call~\cc{g()}, while
  an instance of class~\cc{$γ$2} returns a value of our~\kk{private}
  error type~\cc{$Γ'$.¤} upon the same call.

Three recurring idioms employed in~\cref{Figure:unary-function\ReplaceInThesis{}{-b}}\ReplaceInThesis{(b)}{} are:
\begin{enumerate}
  \item An~\kk{abstract} class encodes a set (alternatively, one can use \kk{interface}s).
    Abstract classes that extend it encode
      subsets, while~\kk{final} classes encode set members.
  \item The interest of frugal management of name-spaces is served
    by the agreement that if a class~\cc{$X$}~\kk{extends} another class~\cc{$Y$}, then~\cc{$X$} is also defined
    as a~\kk{static} member class of~$Y$.
  \item Bodies of functions are limited to a single~\kk{return}~\kk{null}\cc{;} command
      (with interfaces the method body is redundant).
      This is to stress that at runtime, the code does not carry out any useful or interesting computation,
      and the class structure is solely for providing compile-time type checks.
†{%
A consequence of these idioms is that the augmented class~\cc{$Γ'$} is visible to clients.
It can be made~\cc{private}. Just move class~\cc{$Γ$} to outside of~\cc{$Γ'$}, defying the second idiom.
}
\end{enumerate}

Having seen how inheritance and overriding make possible
  the encoding of unary functions, we turn now to encoding higher arity functions.
With the absence of multi-methods, other techniques must be used.

Consider the partial binary function~$f: R⨉S↛Γ$, defined by
\begin{equation}
  \label{Equation:simple-binary}
  \begin{array}{*3c}
    R=❴r₁,r₂❵ & f(r₁,s₁)=γ₁ & f(r₂,s₁)=γ₁⏎
    S=❴s₁,s₂❵ & f(r₁,s₂)=γ₂ & f(r₂, s₂)=⊥
  \end{array}.
\end{equation}
A \Java type encoding of this definition of function~$f$
is in~\cref{Figure:simple-binary\ReplaceInThesis{}{-a}}\ReplaceInThesis{(a)}{}; use cases
are in~\cref{Figure:simple-binary\ReplaceInThesis{}{-b}}\ReplaceInThesis{(b)}{}.

\ReplaceInThesis{%
\begin{figure}[hbt]
  \caption[Type encoding of partial binary function]{\label{Figure:simple-binary}%
    Type encoding of partial binary function~$f:R⨉S↛Γ$,
    where~$R=❴r₁,r₂❵$,~$S=❴s₁,s₂❵$, and~$f$
    is specified by~$f(r₁,s₁)=γ₁$,~$f(r₁,s₂)=γ₂$,~$f(r₂,s₁)=γ₁$, and~$f(r₂, s₂)=⊥$.}
  \begin{tabular}{cc}
    \hspace{-3.5ex}
    \parbox[c]{0.57\linewidth}{%
    \javaInput[]{binary-function.listing}
    }
      &
    \hspace{-16ex}
    \parbox[c]{51ex}{\javaInput[minipage,leftrule=3pt,toprule=3pt,bottomrule=3pt,width=51ex]{binary-function-example.listing}}
⏎
    \parbox{0.57\linewidth}
    {\textbf{(a)} implementation (except for classes~\cc{$Γ$},~\cc{$Γ'$},~\cc{$γ$1}, and~\cc{$γ$2},
    found in \cref{Figure:unary-function}).}
      & \hspace{-5ex}\textbf{(b)} use cases⏎
  \end{tabular}
\end{figure}
}{%
\begin{figure}[hbt]
  \caption[Type encoding of partial binary function]{%
    Type encoding of partial binary function~$f:R⨉S↛Γ$,
    where~$R=❴r₁,r₂❵$,~$S=❴s₁,s₂❵$, and~$f$
    is specified by~$f(r₁,s₁)=γ₁$,~$f(r₁,s₂)=γ₂$,~$f(r₂,s₁)=γ₁$, and~$f(r₂, s₂)=⊥$
     (except for classes~\cc{$Γ$},~\cc{$Γ'$},~\cc{$γ$1}, and~\cc{$γ$2},
    found in \cref{Figure:unary-function-b}) }
    \label{Figure:simple-binary-a}%
    \javaInput[minipage,width=\linewidth]{binary-function.listing}
\end{figure}
\begin{figure}[hbt]
  \caption[Use cases of partial binary function]{%
    Use cases of partial binary function defined in \cref{Figure:simple-binary-a} 
  }
    \label{Figure:simple-binary-b}
    \javaInput[minipage,left=-2ex,width=\linewidth]{binary-function-example.listing}
\end{figure}
}

As the figure shows, to compute~$f(r₁,s₁)$ at compile time we write~\cc{f.r1().s1()}.
Also, the fluent API call chain~\cc{f.r2().s2().g()} results in a compile time
error because 
\[
    f(r₂, s₂)=⊥.
\]

Class~\cc{f} in the implementation sub-figure serves as
  the starting point of the little fluent API defined here.
The return type of~\kk{static} member functions~\cc{r1()} and~\cc{r2()}
  is the respective sub-class of class~\cc{R}:
The return type of function~\cc{r1()} is class~\cc{R.r1};
  the return type of function~\cc{r2()} is class~\cc{R.r2}.

Instead of representing set~$S$ as a class,
  its members are realized as methods~\cc{s1()} and~\cc{s2()} in class~\cc{R}.
These functions are defined as~\kk{abstract} with return type~\cc{$Γ$'}
  in~\cc{R}.
Both functions are overridden in classes~\cc{r1} and~\cc{r2},
   with the appropriate covariant change of their return type,

It should be clear now that the encoding scheme presented
in \Cref{Figure:simple-binary\ReplaceInThesis{}{-a}} can be generalized to functions
  with any number of arguments, provided that the domain and range sets are finite.
The encoding of sets of unbounded size require means for creating an unbounded
 number of types.
Genericity can be employed to serve this end.

\ReplaceInThesis{%
\begin{wrapfigure}[8]{r}{31ex}
  \intextsep=0pt
  \caption{\label{Figure:id}%
  Covariant return type of function \cc{id()}
  with \Java generics.
  }
  \javaInput[,minipage,width=31ex]{id.listing}
\end{wrapfigure}
}{%
\begin{figure}[ht]
  \caption{\label{Figure:id}%
    Covariant return type of function \cc{id()}
    with \Java generics.  }
  \javaInput[minipage,left=-2ex,width=\linewidth]{id.listing}
\end{figure}
}

\Cref{Figure:id} shows a genericity based recipe for
  a function whose return type
  is the same as the receiver's type.
  This recipe is applied in the figure to classes~\cc{A},~\cc{B}, and~\cc{C}.
  In each of these classes, the return type of \cc{id} is,
  without overriding, (at least) the class itself.

It is also possible to encode with \Java generic types
  unbounded data structures, 
  as demonstrated in \cref{Figure:stack-use-cases},
  featuring a use case of a stack of an \emph{unbounded} depth.
\vspace{2ex}

\begin{figure}[ht]
  \caption{\label{Figure:stack-use-cases}%
    Use cases of a compile-time stack data structure.
  }
  \javaInput[minipage,,listing style=numbered]{stack-use-cases.listing}
\end{figure}

In line 3 of the figure, a stack with five elements is created:
These are popped in order (ll.4--7,l.9).
Just before popping the last item, its value is examined (l.8).
Trying then to pop from an empty stack (l.10), or to examine its top (l.11), ends with
  a compile time error.

Stack elements may be drawn from some abstract set~$Γ$.
In the figure these are either class~\cc{$γ$1}
or class \cc{$γ$2} (both defined in \cref{Figure:unary-function\ReplaceInThesis{}{-b}}).
A call to function \cc{$γ$}$i$ pushes the type \cc{$γ$}$i$
  into the stack, for~$i=1,$.
The expression
\[
  \cc{Stack.empty.$γ$1().$γ$1().$γ$2().$γ$1().$γ$1()}
\]
represents the sequence of pushing the value~$γ₁$ into an
empty stack, followed by~$γ₁$,~$γ₂$,~$γ₁$, and, finally,~$γ₁$.
This expression's type is that of variable~\cc{\_1}, i.e.,⏎
\[
 \cc{P<$γ$1,P<$γ$1,P<$γ$2,P<$γ$1,P<$γ$1,E>>>>>}
\]

A recurring building block occurs in this type: 
  generic type~\cc{P}, \emph{short for ‟Push”}, which takes two parameters:
  \begin{enumerate}
    \item the \emph{top} of the stack, always a subtype of~\cc{$Γ$},
    \item the \emph{rest} of the stack, which can be of two kinds:
          \begin{enumerate}
            \item another instantiation of~\cc{P} (in most cases),
            \item non-generic type~\cc{E}, \emph{short for ‟Empty”}, which encodes the empty
              stack. Note that \cc{E} can only occur at the deepest~\cc{P}, encoding a stack 
              with one element, in which the rest is empty. 
          \end{enumerate} %
  \end{enumerate}
Incidentally, \kk{static} field \cc{Stack.empty} is of type~\cc{E}.

\Cref{Figure:stack-encoding\ReplaceInThesis{}{-a}}\ReplaceInThesis{(a)}{} gives the type inheritance hierarchy 
of type \cc{Stack} and its subtypes.
\Cref{Figure:stack-encoding\ReplaceInThesis{}{-b}}\ReplaceInThesis{(b)}{}
gives the implementation of these types.
\ReplaceInThesis{%
\begin{figure}[!htb]
  \caption{Type encoding of an unbounded stack data structure}
  \label{Figure:stack-encoding}
  \begin{tabular}{cc}
    \parbox[c]{0.3\linewidth}{%
      \input{../Figures/stack-classification.tikz}
    } &
    \hspace{-3ex} \parbox[c]{63ex}{\javaInput[minipage,]{stack.listing}}⏎
    \textbf{(a)} type hierarchy &
    \hspace{-3ex} \parbox[t]{63ex}{%
    \textbf{(b)} implementation (except
    for classes~\cc{$Γ$},~\cc{$Γ'$},~\cc{$γ$1}, and~\cc{$γ$2}, which is in \cref{Figure:unary-function}).}
  \end{tabular}
\end{figure}
}{%
\begin{figure}[ht]
  \caption{Type hierarchy of an unbounded stack data structure}
  \label{Figure:stack-encoding-a}
    \centering  \input{../Figures/stack-classification.tikz}
\end{figure}
\begin{figure}[ht]
  \caption[Type encoding of an unbounded stack data structure]
    {Type encoding of an unbounded stack data structure (except
    for classes~\cc{$Γ$},~\cc{$Γ'$},~\cc{$γ$1}, and~\cc{$γ$2},
    which is in \cref{Figure:unary-function-b})}
  \label{Figure:stack-encoding-b}
    \javaInput[minipage,left=-3ex,width=\linewidth]{stack.listing}
\end{figure}
}
The code in the figure shows that the ‟rest” parameter of~\cc{P} must extend class \cc{Stack},
  and that both types~\cc{P} and~\cc{E} extend \cc{Stack}.
Other points to notice are:
\begin{itemize}
  \item The type at the top of the stack is precisely the return type of \cc{top()};
        it is overridden in~\cc{P} so that its return type is the first argument of~\cc{P}.
        The return type of \cc{top()} in~\cc{E} is the error value {$Γ'$.¤}.
  \item Pushing into the stack is encoded as functions~\cc{$γ$1()} and~\cc{$γ$2()};
        the two are overridden with appropriate covariant change of the return type in~\cc{P} and~\cc{E}.
  \item Since an empty stack cannot be popped, function \cc{pop()} is overridden in~\cc{E} to return
    the \emph{error} type \cc{Stack.¤}. This type is indeed a kind of a stack, except that each of the four stack
        functions: \cc{top()}, \cc{push()},~\cc{$γ$1()}, and,~\cc{$γ$2()}, return an appropriate error type.
\end{itemize}
In fact, this recursive generic type technique can used to encode S-expressions: In the spirit of
  \cref{Figure:stack-encoding\ReplaceInThesis{}{-b}}, the idea is to make use of a \cc{Cons} generic type
  with covariant \cc{car()} and \cc{cdr()} methods.

A standard technique of template programming in \CC is to encode conditionals with template specialization.
Since \Java forbids specialization of generics, in lieu we use covariant overloading of function
return type (e.g., the return type of \cc{s2()} in \cref{Figure:simple-binary\ReplaceInThesis{}{-a}} and the
  return type of \cc{top()} in \cref{Figure:stack-encoding\ReplaceInThesis{}{-b}}).

\ReplaceInThesis{%
\begin{wrapfigure}[6]r{38ex}
  \caption{\label{Figure:generic} Covariance of parameters to generics}
  \javaInput[minipage,]{mammal.listing}
\end{wrapfigure}
}{%
  \begin{figure}[ht]
  \caption{\label{Figure:generic} Covariance of parameters to generics}
\javaInput[minipage,left=-2ex,width=\linewidth]{mammal.listing}
\end{figure}
}
\cref{Figure:generic} shows that a similar covariant change is possible 
  in extending a generic type.
The type of the parameter \cc{M} 
  to \cc{Heap} is ``\mbox{\cc{?} \kk{extends} \cc{Mammals}}''.
This type is specialized as \cc{School} extends \cc{Heap}:
  parameter \cc{W} of \cc{School} is of type  
  ``\mbox{\cc{?} \kk{extends} \cc{Whales}}''.
Covariant specialization of parameters to generics 
  is yet another idiom for encoding conditionals.

Overloading gives rise to a third idiom for partial emulation of conditionals, as can be seen
  in~\cref{Figure:peep}.

\begin{figure}[htb]%
  \caption{Peeping into the stack}%
  \label{Figure:peep}%
  \lstset{style=numbered}
  \javaInput[minipage,]{peep.listing}
\end{figure}

The figure depicts type \cc{Peep} and overloaded versions of \cc{peep()} which 
      together make it possible to extract the top of the stack.
The first generic parameter to \cc{Peep} is the top of the stack, the second is the stack itself.
Indeed, we see (l.11) that peeping into an empty stack, places a~\cc{?}
  in the first parameter, thanks to the first overloaded version of \cc{peep()} (l.2).

The second overloaded version of \cc{peep()} (ll.3--6) matches
 against all non-empty stacks. 
The return type
 of this version encodes
 in its first parameter
 the top of the stack, and 
  in its second parameter, the parameter's type.
A use case is in line~9.

Let~$τ$ be the type of the top of a given stack.
Then, both \cc{top()} and \cc{peep()} can be used to extract~$τ$.
There is a subtle difference between the two though:
Obtaining~$τ$ from \cc{top()} does
  not make it possible to define variables, function return types, and
  parameters to functions and generics whose type is~$τ$ or depends on it in any way.
However, since \cc{Peep} is a type that receives~$τ$ as parameter,
  the body of \cc{Peep} is free to define e.g., functions signature includes on~$τ$,
  or pass~$τ$ further to other generics.
