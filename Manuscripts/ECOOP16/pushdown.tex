Notions discussed here are probably common knowledge
 (see e.g.,~\cite{Hopcroft:Ullman:01,Linz:2001} for a text book description,
 or~\cite{Autebert:97} for a scientific review).
The purpose here is to set a unifying common vocabulary.

Let~$Σ$ be a finite alphabet of \emph{terminals} (often called input characters or tokens).
A \emph{language} over~$Σ$
  is a subset of~$Σ^*$.
Keep~$Σ$ implicit henceforth.

A \emph{\textbf Nondeterministic \textbf Pushdown \textbf Automaton} (NPDA) is a device for language recognition,
  made of a nondeterministic finite automaton
  and a stack of unbounded depth of (stack) \emph{elements}.
A NPDA begins execution with a single copy of the initial element on the stack.
In each step, the NPDA
  examines the next input token,
  the state of the automaton,
  and the top of the stack.
It then pops the top element from the stack, and nondeterministically chooses which actions of
  its transition function to perform:
  Consuming the next input token,
  moving to a new state,
  or, pushing any number of elements to the stack.
Actually, any combination of these actions may be selected.

The language recognized by a NPDA is the set of strings that it accepts,
  either by reaching an accepting state or by encountering an empty stack.

A \emph{\textbf Context-\textbf Free \textbf Grammar}(CFG) is a formal description of a language.
A CFG~$G$ has three components:~$Ξ$ a set of \emph{variables} (also called nonterminals),
  a unique \emph{start variable}~$ξ∈Ξ$, and a finite set of (production) \emph{rules}.
A rule~$r∈G$ describes the derivation of a variable~$ξ∈Ξ$ into
  a string of \emph{symbols}, where symbols are either terminals or variables.
Accordingly, rule~$r∈G$ is written as~$r=ξ→β$, where~$β∈\left(Σ∪Ξ\right)^*$.
This description is often called BNF\@.
The \emph{language} of a CFG is the set of strings of terminals (and terminals only)
  that can be derived from the start symbol, following any sequence of applications of the rules.
CFG languages make a proper superset of regular languages, and a proper subset of 
  ‟context-sensitive” languages~\cite{Hopcroft:Ullman:01}.

The expressive power of NPDAs and BNFs is the same:
  For every language defined by a BNF, there exists a NPDA that recognizes it.
Conversely, there is a BNF definition for any language recognized by some NPDA.

NPDAs run in exponential deterministic time.
 A more sane, but weaker, alternative is found in LR($1$) parsers,
  which are deterministic linear time and space.
Such parsers employ a stack and a finite automaton structure,
  to parse the input.
 More generally, LR($k$) parsers,~$k>1$, can be defined. These make their
 decisions based on the next~$k$ input character, rather than just the first of these.
 General LR($k$) parsers are rarely used, since they offer essentially
 the same expressive power†{they recognize the same set of languages~\cite{Knuth:65}.},
 at a greater toll on resources (e.g., size of the automaton).
In fact, the expressive power of LR($k$),~$k\ge1$ parsers, is that
 of ‟\emph{\textbf Deterministic \textbf Pushdown \textbf Automaton}” (DPDA),
  which are similar to NPDA, except that their conduct is deterministic.

\begin{Definition}[Deterministic Pushdown Automaton]
  \label{Definition:DPDA}
  \slshape
  A \textit{deterministic pushdown automaton} (DPDA) is a quintuple~$⟨Q,Γ,q₀,A,δ⟩$
  where~$Q$ is a finite set of \emph{states},~$Γ$ is a finite
  \emph{set of elements},~$q₀∈Q$ is the initial state,
  and~$A⊆Q$ is the \emph{set of accepting states} while~$δ$ is
  the \emph{partial state transition function}~$δ:Q⨉Γ⨉(Σ∪❴ε❵)↛Q⨉Γ^*$.
  \par
  A DPDA begins its work in state~$q₀$ with a single designated stack element residing on the stack.
  At each step, the automaton examines: the current state~$q∈Q$, 
  the element~$γ∈Γ$ at the top of the stack, and~$σ$, the next input token, 
  Based on the values of these, it decides how to proceed:
  \begin{quote}
  \begin{enumerate}
    \item If~$q∈A$ and the input is exhausted, the automaton accepts the input and stops.
    \item Suppose that~$δ(q,γ,ε)≠⊥$ (in this case, the definition of a DPDA
          requires that~$δ(q,γ,σ')=⊥$ for all~$σ'∈Σ$), and let~$δ(q,γ,ε)=(q',ζ)$.
          Then the automaton pops~$γ$ and pushes the string of stack
          elements~$ζ∈Γ^*$ into the stack.
    \item If~$δ(q,γ,σ)=(q',ζ)$, then the same happens, but the automaton also
          irrevocably consumes the token~$σ$.
    \item If~$δ(q,γ,ε)=δ(q,γ,σ)=⊥$ the automaton rejects the input and stops.
  \end{enumerate}
  \end{quote}
\end{Definition}

A \emph{configuration} is the pair of the current state and the stack contents.
Configurations represent the complete information on the 
    state of an automaton at any given point during its computation.
A \emph{transition} of a DPDA takes it from one configuration to another.
Transitions which do not consume an input character are called~\emph{$ε$-transitions}.

As mentioned above, NPDA languages are the same as CFG languages.
Equivalently, \emph{DCFG languages} (deterministic context-free grammar languages)
  are context-free languages that are recognizable by a DPDA\@.
The set of DCFG languages is still a proper superset of regular languages,
  but a proper subset of CFG languages.
