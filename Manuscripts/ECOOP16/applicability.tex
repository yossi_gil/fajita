\Cref{Theorem:Gil-Levy} and its proof above provide
  a concrete algorithm for converting an EBNF specification of a fluent API into
its realization:
\begin{quote}
  \begin{enumerate}
    \item Convert the specification into a plain BNF form
    \urlref{http://lampwww.epfl.ch/teaching/archive/compilation-ssc/2000/part4/parsing/node3.html}.
    \item Convert this BNF into a type of DPDA (using parsing algorithms e.g., LR(k), LALR\@, LL(k)). 
        This conversion might 
        fail~\footnote{In the LR case, we know\cite{Knuth:65} there exists an equivalent grammar for which the conversion will succeed}.
    \item Convert this DPDA into a jDPDA. (Conversion is guaranteed to succeed)
    \item Apply the proof to generate appropriate \Java type definitions, making sure to
        augment methods with code to maintain the fluent-call-list.
        Parsing the fluent-call-list can be done either in each method,
        or lazily, when the product of the fluent API call chain is to
        be used.
  \end{enumerate}
\end{quote}
Although possible, a practical tool that uses the proof directly 
  is a challenge. 
Part of the problem is the complexity of the 
  algorithms used, some of which, e.g., the DPDA and jDPDA equivalence have never been 
  implemented.
Yet another issue that clients of compiler-compiler have grown to expect 
  facilities such as means for resolving ambiguities, manipulation 
  of attributes, etc.
Also, for a fluent API to be elegant and useful, 
  it should support method with parameters whose parameters are also defined by a  fluent API:
these two APIs may mutually recursive and even the same. 
Support of these features through four or so algorithmic abstractions 
  may turn out to be a decent engineering task.

\begin{figure}[H]
  \caption{\label{Figure:compiler} Exponential compilation time for a simple \Java program.
  }
  \begin{minipage}{\textwidth}
  \begin{tabular}{@{}c@{}c@{}}
    \hspace{3ex}\parbox[c]{44ex}{\javaInput[minipage,width=40ex,]{compiler.listing}} &
    \hspace{0ex} \parbox[c]{44ex}{\gnuplotloadfile[terminal=pdf,terminaloptions={crop size 2.5in,1.5in color enhanced font ",8" linewidth 1}]{../Figures/kill.gnuplot}}⏎
      \textbf{(a)} Encoding of a binary type tree 
    & 
      \textbf{(b)} Compilation time 
      (sec†{\scriptsize{measured on an Intel i5-2520M CPU @ 2.50GHz~$⨉$4, 3.7GB memory, Ubuntu 15.04 64-bit, \texttt{javac} 1.8.0\_66}})%
    \\ &
     \emph{vs.}
      length of call chain.
  \end{tabular}
  \end{minipage}
\end{figure}


Yet another challenge is controlling the compiler's  
  runtime.
Learning that linear time parsers and lexical analyzers are possible, 
  and being accustomed to seeing these in practice, one 
  may expect the compiler would run in linear, or at least polynomial time. 
As it turns out, this time is exponential in the worst case (at least for \texttt{javac}).
An encoding of a S-expression in type~\cc{Cons} (\cref{Figure:compiler}(a)) 
  is a not terribly complex such worst case.
  
Type \cc{Cons} takes two type parameters, \cc{Car} and \cc{Cdr} (denoting left and right branches).
Denote the return type of \cc{d()} by 
\[
  τ= \cc{Cons< Cons<Car, Cdr>, Cons<Car, Cdr> >}.
\]
Let~$σ$ denote the type of the \kk{this} implicit parameter to~\cc{d}.
Now, since~$τ= \cc{Cons<}σ,σ\cc{>}$, we have~$|τ|≥2|σ|$,
  where the size of a type is measured, e.g., in number of characters in its textual representation.
Therefore, in a chain of~$n$ calls to \cc{d()}
\begin{equation}
  \label{Equation:n}
  \cc{(Cons<?,?>(null)).}\overbrace{\cc{d().}⋯\cc{.d()}}^{\text{$n$ times}}\cc{;}
\end{equation}
the size of the resulting type is~$O(2ⁿ)$.


\Cref{Figure:compiler}(b) shows, on the doubly logarithmic plane, the runtime
(on a Lenovo X220) of the \texttt{javac} compiler (version 1.8.0\_66) in face
of a \Java program assembled from \cref{Figure:compiler} and \cref{Equation:n}
placed as the single command of \cc{main()}.  Exponential growth is
demonstrated by the right-hand side of the plot, in which curve converges on a
straight line.  (In fact, a variation of the construction may lead to even
super-exponential growth rate of the size of types.)

We believe that this exponential growth is due to a design flaw in the
compiler.  Had the compiler used a representation of types that allows sharing
of expression types, compilation time would be linear. 

Still, with current compiler technology, the type encoding scheme demonstrated
in \cref{Figure:A} might not be scalable.
