Let~$\textsf{java}$ be a function that translates a terminal~$σ∈Σ$
into a call to a uniquely named function (with respect to~$σ$).
Let~$\textsf{java}(α)$, be the function
  that translates a string~$α∈Σ^*$ into a fluent API call chain.
  If~$α=σ₁⋯σₙ∈Σ^*$, then \[
  \textsf{java}(α)=\textsf{java}(σ₁)\cc{().}⋯\cc{.}\textsf{java}(σₙ)\cc{()}
\]
For example, when~$Σ=❴a,b,c❵$ let~$\textsf{java}(a)=\cc{a}$,~$\textsf{java}(b)=\cc{b}$, and,~$\textsf{java}(c)=\cc{c}$.
With these, \[
    \textsf{java}(caba) = \cc{c().a().b().a()}
  \]

\begin{theorem}\label{Theorem:Gil-Levy}
  Let~$A$ be a DPDA recognizing a language~$L⊆Σ^*$.
  Then, there exists a \Java type definition,~$J_A$ for types~\cc{L},~\cc{A} and
    other types such that the \Java command
  \begin{equation}
    \label{Equation:result}
    \cc{L~$ℓ$ = A.build.$\textsf{java}(α)$}\cc{.\$();}
  \end{equation}
  type checks against~$J_A$ if an only if~$α∈L$.
  Furthermore, program~$J_A$ can be effectively generated from~$A$.
\end{theorem}

\Cref{Equation:result} reads: starting from the \kk{static} field \cc{build} of \kk{class}~\cc{A},
  apply the sequence of call chain~$\textsf{java}(α)$, terminate with a call to the
  ending character~\cc{\$()} and then assign to newly declared \Java variable~\cc{$ℓ$} of type~\cc{L}.

The proof of the theorem is by a scheme for encoding in \Java types
  the pushdown automaton~$A=A(L)$ that recognizes language~$L$.
Concretely, the scheme assigns a type~$τ(c)$
  to each possible configuration~$c$ of~$A$.
Also, the type of \cc{A.build} is~$τ(c₀)$, where~$c₀$ is the initial configuration of~$A$,

Further, in each such type the scheme places
  a function~$σ()$ for every~$σ∈Σ$.
Suppose that~$A$ takes a transition from configuration~$cᵢ$ to configuration~$cⱼ$
  in response to an input character~$σₖ$.
Then, the return type of function \cc{$σₖ$()} in type~$τ(cᵢ)$ is type~$τ(cⱼ)$.

With this encoding the call chain in \cref{Equation:result}
  mimics the computation of~$A$, starting at~$c₀$ and ending with
  rejection or acceptance.
The full proof is in \cref{section:proof}.

Since the depth of the stack is unbounded, the number of configurations of $A$ is unbounded,
  and the scheme must generate an infinite number of types.
Genericity makes this possible, since a generic type is
  actually device for creating an unbounded number of types.

There are several, mostly minor, differences between the structure of the \Java code
in \cref{Equation:result}
and the examples of fluent API we saw above\ReplaceInThesis{,e.g., in \cref{figure:DSL}}{}:
\begin{description}
  \item[Prefix, i.e., the starting \cc{A.build} variable.]
  All variables and functions of \Java are defined within a class.
  Therefore, a call chain must start with an object (\cc{A.build} in \cref{Equation:result})
  or, in case of \cc{static} methods, with the name of a class.
  In fluent API frameworks this prefix is typically eliminated
  with appropriate \cc{import} statements.
  \par
  If so desired, the same can be done by our type encoding scheme: define all
  methods in type~$τ(c₀)$ as \cc{static} and \cc{import static} these.
  \item[Suffix, i.e., the terminal \cc{.\$()} call.]
  In order to know whether~$α∈L$ the automaton recognizing~$L$ must
  know when~$α$ is terminated.
  \par
  With a bit of engineering, this suffix can also be eliminated.
  One way of doing so is by defining type~\cc{L} as an \kk{interface}, and by making all types~$τ(c)$,~$c$ is
  an accepting configuration, as subtype of~\cc{L}.
  \item[Parameterized methods.]
  Fluent API frameworks support call chains with phrases such as:
  \begin{itemize}
    \item ‟\lstinline{.when(header(foo).isEqualTo("bar")).}”,
    \item ‟\lstinline{.and(BOOK.PUBLISHED.gt(date("2008-01-01"))).}”, and,
    \item ‟\lstinline{.allowing(any(Object.class)).}”.
  \end{itemize}
  while our encoding scheme assumes methods with no parameters.  
  \par
    Methods with parameters contribute to the user
      experience and readability of fluent APIs but their ``computational expressive power"' is the same.
      In fact, extending
      \cref{Theorem:Gil-Levy} to support these requires these conceptually simple steps 
      \begin{enumerate}
        \item Define the structure of parameters to methods with appropriate fluent API, which may or
          may not be, the same as the fluent API of the outer chain, or the fluent API of parameters to
          other methods. Apply the theorem to each of these fluent APIs.
        \item
          If there are several overloaded versions of a method, consider each such version as a distinct
          character in the alphabet~$Σ$ and in the type encoding of the automaton.
        \item
          Add code to the implementation of each method code to store the 
          value of its argument(s) in a record placed at the end of the fluent-call-list. 
      \end{enumerate}
\end{description}
