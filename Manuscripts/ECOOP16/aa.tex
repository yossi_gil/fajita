Ever after their inception\urlref{http://martinfowler.com/bliki/FluentInterface.html} \emph{fluent APIs}
  increasingly gain popularity~\cite{Hibernate:06,Freeman:Pryce:06,Larsen:2012} and research
  interest~\cite{Deursen:2000,Kabanov:2008}.
In many ways, fluent APIs are a kind of
  \emph{internal} \emph{\textbf Domain \textbf Specific \textbf Language}:
They make it possible to enrich a host programming language without changing it.
Advantages are many: base language tools (compiler, debugger, IDE, etc.) remain
  applicable, programmers are saved the trouble of learning a new syntax, etc.
However, these advantages come at the cost of expressive power;
  in the words of Fowler:
  ‟\emph{Internal DSLs are limited by the syntax and structure of your base language.}”†
  {M. Fowler, \emph{Language Workbenches: The Killer-App for Domain Specific Languages?},
    2005
    \newline
  \url{http://www.martinfowler.com/articles/languageWorkbench.html＃InternalDsl}}.
Indeed, in languages such as \CC, fluent APIs
  often make extensive use of operator overloading (examine, e.g., \textsf{Ara-Rat}~\cite{Gil:Lenz:07}),
  but this capability is not available in \Java.

Despite this limitation, fluent APIs in \Java can be rich and expressive, as demonstrated
  in \cref{figure:DSL} showing use cases of the DSL of Apache Camel~\cite{Ibsen:Anstey:10}
(open-source integration framework),
and that of jOOQ\urlref{http://www.jooq.org}, a framework for writing
  SQL in \Java, much like Linq~\cite{Meijer:Beckman:Bierman:06}.

\begin{figure}[H]
  \caption{\label{figure:DSL} Two examples of \Java fluent API}
  \begin{tabular}{@{}c@{}c@{}}
    \parbox[c]{44ex}{\javaInput[]{../Fragments/camel-apache.java.fragment}} &
    \hspace{-3ex} \parbox[c]{59ex}{\javaInput[]{../Fragments/jOOQ.java.fragment}}⏎
    \textbf{(a)} Apache Camel & \textbf{(b)} jOOQ
  \end{tabular}
\end{figure}

Other examples of fluent APIs in \Java are abundant:
  jMock~\cite{Freeman:Pryce:06},
  Hamcrest\urlref{http://hamcrest.org/JavaHamcrest/},
  EasyMock\urlref{http://easymock.org/},
  jOOR\urlref{https://github.com/jOOQ/jOOR},
  jRTF\urlref{https://github.com/ullenboom/jrtf}
  and many more.

\subsection{A Type Perspective on Fluent APIs}
\Cref{figure:DSL}(B) suggests that jOOQ imitates SQL,
but, is it possible at all to produce a fluent API
for the entire SQL language,
or XPath, HTML, regular expressions, BNFs, EBNFs, etc.?
Of course, with no operator overloading it is impossible
to fully emulate tokens; method names though make a good substitute for tokens, as done
in ‟\lstinline{.when(header(foo).isEqualTo("bar")).}” (\cref{figure:DSL}).
The questions that motivate this research are:
\begin{quote}
  \begin{itemize}
    \item Given a specification of a DSL, determine whether there exists
        a fluent API that can be made for this specification?
    \item In the cases that such fluent API is possible,
      can it be produced automatically?
    \item Is it feasible to produce a \emph{compiler-compiler} such as Bison~\cite{Bison:manual}
        to convert a language specification into a fluent API?
\end{itemize}
\end{quote}

Inspired by the theory of formal languages and automata,
  this study explores what can be done with fluent APIs in \Java.

Consider some fluent API (or DSL) specification, permitting only certain call
chains and disallowing all others.
Now, think of the formal language that defines the set of these permissible chains.
We prove that there is always a \Java type definition that
  \emph{realizes} this fluent definition, provided that this
  language is \emph{deterministic context-free}, where
\begin{itemize}
  \item In saying that a type definition \emph{realizes} a specification of fluent
    API, we mean that call chains that conform with the API definition compile
    correctly, and, conversely, call chains that are forbidden by the API
    definition do not type-check, resulting in an appropriate compiler error.
  \item Roughly speaking, deterministic context-free languages are those
    context-free languages that can be recognized by an LR parser†{The ‟L"
    means reading the input left to right; the ‟R" stands for rightmost derivation}~\cite{Aho:Sethi:Ullman:86}.
    \par
    An important property of this family is that none of its members is ambiguous.
    Also, it is generally believed that most practical programming languages
    are deterministic context-free.
\end{itemize}

A problem related to that of recognizing a formal language,
is that of parsing, i.e., creating, for input which is within the language,
  a parse tree according to the language's grammar.
In the domain of fluent APIs, the distinction between recognition and parsing is
  in fact the distinction between compile time and runtime.
Before a program is run, the compiler checks whether the fluent API call is legal,
  and code completion tools will only suggest legal extensions of a current call chain.

In contrast, a parse tree can only be created at runtime.
Some fluent API definitions create the parse-tree
  iteratively, where each method invocations in the call chain adding
  more components to this tree.
However, it is always possible to generate this tree in ‟batch” mode:
This is done by maintaining a \emph{fluent-call-list} which
  starts empty and grows at runtime by having each method invoked add to it
  a record storing the method's name and values of its parameters.
The list is completed at the end of the fluent-call-list, at which point it is fed to an appropriate parser that
  converts it into a parse tree (or even an AST).

\subsection{Contribution}
The answers we provide for the three questions above are:
\begin{quote}
  \begin{enumerate}
  \item If the DSL specification is that of a deterministic context-free
    language, then a fluent API exists for the language, but we do not know
    whether such a fluent API exists for more general languages.
  \par
  Recall that there are universal cubic time parsing
  algorithms~\cite{Cocke:1969,Earley:1970,Younger:1967} which can parse (and recognize) any
  context-free language. What we do not know is whether algorithms of this sort
  can be encoded within the framework of the \Java type system.
  \item
  There exists an algorithm to generate a fluent API that realizes any
  deterministic context-free languages. Moreover, this fluent API can create
  at runtime, a parse tree for the given language. This parse tree can then be
  supplied as input to the library that implements the language's semantics.
  \item
  Unfortunately, a general purpose compiler-compiler
  is not yet feasible with the current algorithm.
  \begin{itemize}
    \item One difficulty is usual in the fields of formal languages:
      The algorithm is complicated and relies on
      modules implementing complicated theoretical results, which, to the best of our
      knowledge, have never been implemented.
    \item Another difficulty is that a certain design decision in the
      implementation of the standard \texttt{javac} compiler is likely to make it choke on the
      \Java code generated by the algorithm.
  \end{itemize}
  \end{enumerate}
\end{quote}

Other concrete contributions made by this work include
\begin{itemize}
  \item the understanding that the definition of fluent APIs is analogous to
      the definition of a formal language.
  \item a lower bound (deterministic pushdown automata)
    on the theoretical ‟computational complexity” of the \Java type system.
  \item an algorithm for producing a fluent API for deterministic context-free languages.
  \item a collection of generic programming techniques, developed towards this algorithm.
  \item a demonstration that the runtime of Oracle's \texttt{javac} compiler may be exponential in the program size.
\end{itemize}

\subsection{Related Work}

It has long been known
  that \CC templates are Turing complete in the following precise sense:

\begin{Proposition}
  \label{Theorem:Gutterman}
  For every Turing machine,~$m$, there exists a \CC program,~$Cₘ$ such that
    compilation of~$Cₘ$ terminates if and only if
      Turing-machine~$m$ halts.
      Furthermore, program~$Cₘ$ can be effectively generated from~$m$~\cite{Gutterman:2003}.
\end{Proposition}

Intuitively, this is due to the fact that templates in \CC
  feature both recursive invocation and conditionals (in the form of
  ‟\emph{template specialization}”).

In the same fashion, it should be mundane to make the judgment that
  \Java's generics are not Turing-complete since they offer no conditionals.
Still, even though there are time complexity results regarding type systems in functional
  languages, we failed to find similar claims for \Java.

Specialization, conditionals, \kk{typedef}s and other features of \CC templates,
  gave rise to many advancements in template/generic/generative programming
  in the language~\cite{Austern:98,Musser:Stepanov:1989,Backhouse:Jansson:1999,Dehnert:Stepanov:2000},
  including e.g., applications in numeric libraries~\cite{Veldhuizen:95,Vandevoorde:Josuttis:02},
  symbolic derivation~\cite{Gil:Gutterman:98}
  and a full blown template library~\cite{Abrahams:Gurtovoy:04}.

Garcia et al.~\cite{Garcia:Jarvi:Lumsdaine:Siek:Willcock:03} compared
  the expressive power of generics in half a dozen major programming languages.
  In several ways, the \Java approach~\cite{Bracha:Odersky:Stoutamire:Wadler:98}
  did not rank as well as others.

Not surprisingly, work on meta-programming using \Java generics,
  research concentrating on other means for enriching the language,
  most importantly annotations~\cite{Papi:08}.

The work on SugarJ~\cite{Erdweg:2011} is only one of many other attempts
  to achieve the embedded DSL effect of fluent APIs by language extensions.

Suggestions for semi-automatic generation can be found in the work of Bodden~\cite{Bodden:14} and
  on numerous locations in the web.
None of these materialized into an algorithm or analysis of complexity.
However, there is a software artifact (fluflu\urlref{https://github.com/verhas/fluflu})
  that automatically generates a fluent API that obeys the transitions
    of a given finite automaton.
