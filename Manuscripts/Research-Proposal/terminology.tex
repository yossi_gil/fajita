\subsection{Method Chaining}
The term \emph{method chaining} is illustrated in neat examples, such
as in the \nth{3} to \nth{6} line of the following \Java function:
\begin{Code}{JAVA}{Method chaining}
String time(int hours, int minutes, int seconds) {¢¢
  StringBuilder sb = new StringBuilder();
    return sb
      ¢¢.append(hours).append(':')
      ¢¢.append(minutes).append(':')
      ¢¢.append(seconds)
      ¢¢.toString();
}
\end{Code}
Method chaining is to say that the same object, \cc{sb} of type \cc{StringBuilder} in the above,
   is the receiver of a chain of methods.
This is achieved by making each variant of the method \cc{append} return the receiver, denoted by the
  implicit argument \kk{this}.
With this convention, the tedious code
\begin{JAVA}
sb.append(hours);
sb.append(':');
sb.append(minutes);
sb.append(':');
sb.append(seconds);
return sb.toString():\end{JAVA}
becomes a bit shorter and less repetitive.

\subsection{Fluent API}
Now, definition found in the web of the term \emph{fluent API†{API =
    \textbf Application \textbf Program \textbf Interface
}} are a bit illusive.
Invariably, such definitions liken fluent API to method chaining;
  they also tend to emphasize that fluent API is ‟more” than method
  chaining, and ask the reader to extrapolate from one term to the other.

Indeed, fluent API looks much like method chaining; indeed method chaining is a kind
  of fluent API{}.
However, the main distinction between the two terms is the identity of the receiver.
In method chaining, all methods are invoked on the same object, whereas in fluent API
  the receiver of each method in the chain may be arbitrary.
Perhaps surprisingly, this difference makes fluent API significantly more expressive.
Consider, for example, the following code fragment (drawn from JMock~\cite{Freeman:Pryce:06})
\begin{Code}{Java}{Fluent API}
allowing (any(Object.class))
  ¢¢.method("get.*")
  ¢¢.withNoArguments();
\end{Code}
Let the return type of function \cc{allowing} be denoted by~$τ₁$ and let the return type of function \cc{method} be denoted by~$τ₂$.
Then, the fact that~$τ₁≠τ₂$ means that the set of methods that can be placed after the dot
in the partial call chain
\begin{code}{Java}
 allowing(any(Object.class)).
\end{code}
is distinct from the set of methods that can be placed after the dot in the partial call chain
\begin{code}{Java}
allowing(any(Object.class)).method("get.*").
\end{code}
This distinction make it possible to design expressive and rich fluent APIs, in which a sequence ‟chained” calls is not only readable, but also
robust, in the sense that the sequence is type correct, only when it the sequence makes sense semantically.

\subsection{The Type Safe Toilette Seat}

An object of type toilette seat is created in the \cc{down} state, but it can
then be \cc{raise}d to the \cc{up} state, and then be \cc{lower}ed to the
\cc{down} state.†{%
  This example is inspired by earlier work of
  Richard Harter on the topic~\cite{Harter:05}.
}
Such an object be used by two kinds of users, \cc{male}s and \cc{female}s, for two distinct purposes: \cc{urinate} and \cc{defecate}.
Now a good fluent API design is one by which the sequences of method calls in
\cref{Figure:toilette:legal} are type correct.

\begin{figure}[htbp]
  \begin{JAVA}
new Seat().male().raise().urinate();
new Seat().female().urinate();\end{JAVA}
  \caption{Legal sequences of calls in the toilette seat example}
  \label{Figure:toilette:legal}
\end{figure}

Conversely, sequences of method calls made in \cref{Figure:toilette:illegal} 
  should be illegal in the desired implementation of the fluent API.

\begin{figure}[htbp]
  \begin{JAVA}
new Seat().female().raise();
new Seat().male().raise().defecate();
new Seat().male().male();
new Seat().male().raise().urinate().female().urinate();\end{JAVA}
  \caption{Illegal sequences of calls in the toilette seat example}
  \label{Figure:toilette:illegal}
\end{figure}
It should be clear that the type checking engine of the compiler can
be employed to distinguish between legal and illegal sequences.
It should also be clear that fabricating the \kk{class}es, \kk{interface}s
and the \kk{extends} and \kk{implements} relationships between these, is
far from being trivial.

\subsection{Type State}
The toilette seat problem may be amusing to some, but it is not contrived in
any way.  In fact, there is huge body of research on the general topics of
\emph{type-states}. (See e.g., review articles such
as~\cite{Aldrich:Sunshine:2009,Bierhoff:Aldrich:2005}) Informally, an object
that belongs to a certain type (\kk{class} in the object oriented lingo), has
type-states, if not all methods defined in this object's class are applicable
to the object in all states it may be in.

A classical example of type-states is a file object: which can be in one of two
states: ‟open” or ‟closed”. Invoking a \cc{read()} method on the object is only
permitted when the file is in an ‟open” state.  In addition, method \cc{open()}
(respectively \cc{close()}) can only be applied if the object is in the
‟closed” (respectively, ‟open”) state.

Objects with type states such as toilette seats and files are not rarities.
In fact, a recent study~\cite{Beckman:2011} estimates 
  that about 7.2％ of \Java classes define protocols, that can be interpreted as type-state.
Type-state pose two main challenges to software engineering 
\begin{enumerate}
  \item \emph{\textbf{Identification.}} 
    In the typical case, type-state 
        receive little to no mention at all in the documentation.
    The identification problem is to find the implicit 
    type state in existing code: Given an implementation of a class
    (or more generally of a software framework), 
    \emph{determine} which sequences of method calls are valid and which violate the 
    type state hidden in the code.
  \item \emph{\textbf{Maintenance and Enforcement.}}
    Having identified the type-states, the challenge is in automatically flagging out 
      illegal sequence of calls that does not conform
      with the type-state, furthermore, with the 
      evolution of an API, the challenge is in updating the type-state information, 
      and the type checking of code of clients. 
\end{enumerate}
