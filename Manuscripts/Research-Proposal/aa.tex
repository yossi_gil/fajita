Modern programming languages acquire high-level constructs
  at a staggering rate.
The imminent adoption of closures in \Java and \CC,
  the generators of \CSharp, and ‟concepts” in
  \CC are just a few examples.

A theoretical motivation for this work
  is the exploration of the computational
  expressiveness of such features.
For example, it is known (see e.g.,~\cite{Gil:Gutterman:2003}) that
  \kk{template}s are Turing complete in the following precise sense:

\begin{Theorem}
  \label{Theorem:Gutterman}
  For every Turing machine~$m$†{We tacitly assume that a Turing machine
    encodes in it its input}, there exists a \CC program,~$Cₘ$ such that
  \begin{quote}
    compilation of~$Cₘ$ of terminates if and only if
    \begin{quote}
      Turing-machine~$m$ halts.
    \end{quote}
  \end{quote}
  Furthermore, program~$Cₘ$ can be effectively†{in the computability lingo:
    effective~$≈$‟~\emph{there is time
  bounded algorithm for carrying out the task}”.} generated from~$m$.
\end{Theorem}

Intuitively, the proof relies on the fact that \kk{template}s
  feature recursive invocation and conditionals (in the form of
  ‟\emph{template specialization}”.

In the same fashion, it is mundane to make the judgment that
  \Java's generics are not Turing-complete: all recursive calls
  in these are unconditional.
In one sense, my research shall give a lower bound on the
  expressive power of \Java generics in terms of the Chomsky hierarchy~\cite{Chomsky:1963}.
I hope to show that it is possible to encode context free grammars within the \Java type system,
  in such a way that type-checking is equivalent to membership queries.
This objective is more precisely expressed in the following conjecture.

\begin{Conjecture}
\label{Conjecture:Gil:Levy}
Let~$Σ$ be finite alphabet.
Let~$M$ be a \emph{push-down automaton(PDA)}, and let~$𝓛ₛ⊆Σ^*$ be
the \emph{formal language} such that~$M$ recognizes.
Then, there exists a \Java program,~$Jₛ$ and a location~$ℓ∈Jₛ$
such that
\begin{quote}
  if a string~$α∈Σ$ is
  placed at location~$ℓ$ in~$Jₛ$ then
  \begin{quote}
    program~$Jₛ$ type checks, if an only if
    \[
      α∈𝓛ₛ.
    \]
  \end{quote}
\end{quote}
Furthermore, program~$Jₛ$ can be effectively generated from~$M$.
\end{Conjecture}

The proof shall be made with an algorithm that converts a 
  PDA~$M$ into a program~$Jₛ$.
I am currently in the process of implementing a program that
  does precisely that.
This program, along with its API, other lesser components and
  some connecting tissue, are to be known as \Self.

The above discussion, formalism and conjecture may seem at sight too abstract
  and of little practical value.
Re-positioning perspective may shed a different light:
\cref{Theorem:Gutterman} is important not only because it tells us
  that the question of termination of a \CC compiler is as difficult
  the halting problem~\cite{Turing:1936}, but also because it
  justifies the common belief that is no limit to the power of 
  template programming~\cite{Musser:Stepanov:1989,Dehnert:Stepanov:2000
  ,Backhouse:Jansson:1999, Austern:1998,Bracha:Odersky:1998,Garcia:Jarvi:2003}.

In the fashion, \cref{Conjecture:Gil:Levy} has important
  implications to the emerging trend of employing fluent APIs
  to make complex software systems more accessible.
My thesis is that one can employ the conjecture for
  automatic generation of very rich such APIs.
