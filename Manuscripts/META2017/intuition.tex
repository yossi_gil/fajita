%! TEX root = 00.tex
Formal presentation of the algorithm that compiles an LL(1) grammar into an implementation of a
  language recognizer with \Java generics
  is delayed to~\cref{section:algorithm}.
This section gives an intuitive perspective on this algorithm.

We will first recall the essentials of the classical LL(1) parsing algorithm (\cref{section:essentials}).
Then, we will explain the limitations of the computational model
  offered by \Java generics (\cref{section:limitations}).

The discussion proceeds to the observation
  that underlies our emulation of the parsing algorithm
  within these limitations.

Building on all these, \Cref{section:generation} can
make the intuitive introduction to the main algorithm.
To this end, we revise here the classical
algorithm~\cite{Lewis:66} for converting an LL grammar
(in writing LL, we, here and henceforth really mean
LL(1)), and explain how it is modified to generate a
  recognizer executable on the computational model of
\Java generics.

\subsection{Essentials of LL parsing}
\label{section:essentials}
The traditional \emph{\textbf{LL} \textbf Parser} (\LLp)
  is a DPDA allowed to peek at the next input symbol.
Thus,~$δ$, its transition function, takes two parameters: the current
  state of the automaton, and a peek into the terminal next to be read.
The actual operation of the automaton is by executing in loop the step
  depicted in \cref{algorithm:ll-parser}.

\renewcommand\algorithmicdo{\textbf{\emph{}}}
\renewcommand\algorithmicthen{\textbf{\emph{}}}
\begin{algorithm}
  \caption{\label{algorithm:ll-parser}
  The iterative step of an \LLp}
  \begin{algorithmic}[1]
      \LET{$X$}{\Function pop()}\COMMENT{what's next to parse?}
      \IF[anticipate to match terminal~$X$]{$X∈Σ$}
        \IF[read terminal is not anticipated~$X$]{$X≠\Function next()$}
          \STATE{\textsc{Reject}}\COMMENT{automaton halts in error}
        \ELSE[terminal just read was the anticipated~$X$]
          \STATE{\textsc{Continue}}\COMMENT{restart, popping a new~$X$, etc.}
        \FI
      \ELSIF[anticipating end-of-input]{$X=\$$}
        \IF[not the anticipated end-of-input]{$\$≠\Function next()$}
          \STATE{\textsc{Reject}}\COMMENT{automaton halts in error}
        \ELSE[matched the anticipated end-of-input]
          \STATE{\textsc{Accept}}\COMMENT{all input successfully consumed}
        \FI
      \ELSE[anticipated~$X$ must be a nonterminal]
        \LET{$R$}{$\Function δ(X, \Function peek())$}\COMMENT{determine rule to parse~$X$}
        \IF[no rule found]{$R=⊥$}
          \STATE{\textsc{Reject}}\COMMENT{automaton halts in error}
        \ELSE[A rule was found]
          \LET{$(Z ::= Y₁,…,Yₖ)$}{R}\COMMENT{break into left/right}
          \STATE{\textbf{assert}~$Z=X$}\COMMENT{$δ$ constructed to return valid~$R$ only}
          \STATE{$\Function print(R)$}\COMMENT{rule~$R$ has just been applied}
          \STATE{\textbf{For}~$i=k,…,1$,~$\Function push(Yᵢ)$}\COMMENT{push in reverse order}
          \STATE{\textsc{Continue}}\COMMENT{restart, popping a new~$X$, etc.}
        \FI % No rule found
      \FI % Main
\end{algorithmic}
  \vspace{0.3ex}
  \hrule
  \vspace{0.3ex}

  \ReplaceInThesis{\scriptsize}{}
  \begin{enumerate}
      \item
  Input is a string of terminals drawn from alphabet~$Σ$, ending
    with a special symbol~$\$∉Σ$.
      \item
  Stack symbols are drawn from~$Σ∪❴\$❵∪Ξ$, where~$Ξ$ is
  the set of nonterminals of the grammar from which the automaton was generated.
\item
  Functions~$\Function pop(·)$ and~$\Function push(·)$
    operate on the pushdown stack; function~$\Function next()$ returns
  and consumes the next terminal from the input string;
    function~$\Function peek()$ returns this terminal without consuming it.
\item
  The automaton starts with the stack with the start symbol~$S∈Ξ$ pushed
    into its stack.
  \end{enumerate}
\end{algorithm}

The DPDA maintains a stack of ‟anticipated” symbols, which may
  be of three kinds: a terminal drawn from the input alphabet~$Σ$,
  an end-of-input symbol~$\$$, or one of~$Ξ$, the set of
  nonterminals of the underlying grammar.

If the top of the stack is an input symbol or the special,
 end-of-input symbol~$\$$, then it must match the next terminal
 in the input string, the matched terminal is then consumed from
 the input string.
If there is no match, the parser rejects.
The parser accepts if the input is exhausted with
  no rejections (i.e.,~$\$$ was matched).

The more interesting case is that~$X$, the popped symbol
  is a nonterminal: the DPDA peeks into the next terminal in the input
  string (without consuming it).
Based on this terminal, and~$X$, the transition function~$δ$
  determines~$R$-the derivation rule applied to derive~$X$.
The algorithm rejects if~$δ$ can offer no such rule.
Otherwise, it pushes into the stack, in reverse order, the symbols
  found in the right hand side of~$R$.

\subsection{LL(1) Parsing with \Java Generics?}
\label{section:limitations}
Can \cref{algorithm:ll-parser} be executed on the machinery
  available with \Java generics?
As it turns out, most operations conducted by the algorithm
  are easy.
The implementation of function~$δ$ can
  be found in the toolbox in \cite{Gil:Levy:2016}.
Similarly, any fixed sequence of push and pop
  operations on the stack can be conducted within a \Java
  transition function:
  the ‟\textbf{For}" at the algorithm can be unrolled.

Superficially, the algorithm appears to be doing a constant amount
  of work for each input symbol.
A little scrutiny falsifies such a conclusion.

In the case~$R=X::=Y$,~$Y$ 
  being a nonterminal, the algorithm will conduct
  a~$\Function pop(·)$ to remove~$X$ and a~$\Function push(·)$
  operation for~$Y$ before consuming a terminal from the input.
Further,~$δ$ may return next the rule~$Y::=Z$,
  and then the rule~$Z::=Z'$, etc.
Let~$k'$ be the number of such substitutions
  occurring while reading a single input token.

\begin{Definition}[$k'$ - subtitution factor]
  \label{substitution-factor}
  Let~$A$ be a nonterminal at the top of the stack
    and~$t$ be the next input symbol.
  If~$t \in \Function First(A)$ then~$k'$ is the number of 
  consecutive substitutions the parser will perform 
  (replacing the nonterminal at the top the stack with one of it's rules)
  until~$t$ will be consumed from the input.
\end{Definition}

Also, in the case~$R=X::=ε$ the DPDA does not push
  anything into the stack.
Further, in its next iteration, the DPDA conducts another
pop,
\[
  X'←\Function pop(),
\]
instruction and proceeds to consider this new~$X'$.
If it so happens, it could also be the case
  that the right hand side of rule~$R'$
  \[
    R' = δ(X', \Function peek())
  \]
  is once again empty,
  and then another~$\Function pop()$
    instruction occurs
\[
  X”←\Function pop(),
\]
  etc.
Let~$k^*$ be the number
  of such instructions in a certain such mishap.

\begin{Definition}[$k^*$ - pop factor]
  \label{pop-factor}
  Let~$X$ be a nonterminal at the top of the stack
    and~$t$ be the next input symbol.
  Then~$k^*$ is the number of consecutive substitutions
  the parser will perform \emph{of only~$ε$ rules}
  until~$t$ will be consumed from the input.
\end{Definition}

The~$\Function First(·)$ set of symbol~$X$ is the set of all terminals
that might appear at the beginning of a string derived from~$X$ (algorithm for
computing~$\Function First(·)$ is presented in \cref{algorithm:first}),
thus, for example, if~$X$ is a terminal then its first set is the set
containing only~$X$.
The~$\Function First(·)$ set is extended for strings too,~$\Function First(α)$
contains all terminals that can begin a string derived from~$α$ (Appropriate
algorithm can be found at~\cref{algorithm:first-string}).

The~$\Function Follow(·)$ set of nonterminal~$A$ is the set of all
  terminals that might appear immediately after a string that was
  derived from~$A$ (Appropriate algorithm can be found
  at~\cref{algorithm:follow}).

\begin{algorithm}[H]
  \caption{\label{algorithm:first}
  An algorithm for computing~$\Function First(X)$ for each grammar symbol~$X$
    in the input grammar~$G =⟨Σ,Ξ,P⟩$}
    \begin{algorithmic}
    \FOR[initialize~$\Function First(·)$ for all nonterminals]{$A∈Ξ$}
      \STATE{$\Function First(A)=∅$}
    \ENDFOR
    \FOR[initialize~$\Function First(·)$ for all terminals]{$t∈Σ$}
      \STATE{$\Function First(t)=❴t❵$}
    \ENDFOR
    \WHILE{No changes in any~$\Function First(·)$ set}
      \FOR[for every production]{$X::=Y₀…Yₖ∈P$}
        \STATE{$\Function First(X) \; ∪\! = \Function First(Y₀)$}
        \COMMENT{add~$\Function First(Y₀)$ to~$\Function First(X)$}
        \FOR[for each symbol in the RHS]{$i∈❴0…k❵$}
          \IF[if the prefix is nullable]{$\Function Nullable(Y₀…Y_{i-1})$}
          \STATE{$\Function First(X) \; ∪\! = \Function First(Yᵢ)$}
            \COMMENT{add~$\Function First(Yᵢ)$ to~$\Function First(X)$}
          \ENDIF
        \ENDFOR % Symbols in the right-hand-side of a production
      \ENDFOR % Productions
    \ENDWHILE
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}
  \caption{\label{algorithm:first-string}
  An algorithm for computing~$\Function First(α)$ for some string of symbols~$α$.
  This algorithm relies on results from~\cref{algorithm:first}}
  \begin{algorithmic}
    \LET{$Y₀…Yₖ$}{$α$}\COMMENT{break~$α$ into its symbols}
    \STATE{$\Function First(α) \; ∪\! = \Function First(Y₀)$}
    \COMMENT{initialize~$\Function First(α)$ with~$\Function First(Y₀)$}
    \FOR[for each symbol in the string]{$i∈❴1…k❵$}
      \IF[if the prefix is nullable]{$\Function Nullable(Y₀…Y_{i-1})$}
        \STATE{$\Function First(α) \; ∪ \! = \Function First(Yᵢ)$}
        \COMMENT{add~$\Function First(Yᵢ)$ to~$\Function First(α)$}
      \ENDIF
    \ENDFOR
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}
  \caption{\label{algorithm:follow}
  An algorithm for computing~$\Function Follow(A)$ for each nonterminal~$X$
    in the input grammar~$G =⟨Σ,Ξ,P⟩$ when~$\$∉Σ$
    and~$S∈Σ$ is the start symbol of~$G$}
  \begin{algorithmic}
    \STATE{$\Function Follow(S)=❴ \$ ❵$}
    \COMMENT{initialize the start symbol}
    \WHILE{No changes in any~$\Function Follow(·)$ set}
      \FOR[for each grammar rule]{$A::=Y₀…Yₖ∈P$}
        \FOR[for each symbol in the RHS]{$i∈❴0…k❵$}
          \IF[compute only for nonterminals]{$Yᵢ∉Ξ$}
            \CONTINUE
          \ENDIF
          \STATE{$\Function Follow(Yᵢ) \; ∪ \! = \Function First(Y_{i+1}…Yₖ)$}
          \IF[if the suffix is nullable]{$\Function Nullable(Y_{i+1}…Yₖ)$}
            \STATE{$\Function Follow(Yᵢ) \, ∪ \! = \Function First(A)$}
            \COMMENT{add~$\Function First(A)$}
          \ENDIF
        \ENDFOR % Symbol in the RHS
      \ENDFOR
    \ENDWHILE
  \end{algorithmic}
\end{algorithm}

For example, the nonterminal \<Definitions> can be derived to a string
  beginning with \cc{label} (if there are defined labels), or \cc{const}
  (if there are no labels defined), or \cc{procedure} (if there are no
  labels or constants defined).
$\Function Follow(\<Definitions>)$ on the other hand, contains only
\cc{begin} since~\<Definitions> is always followed by~\<Body>,
  and~$\Function First(\<Body>)$ contains only \cc{begin}.

\subsection{LL(1) parser generator}
\label{section:generation}
The LL(1) parser is based on a prediction table.
For a given nonterminal as the top of the stack, and an input symbol,
  the prediction table provides the next rule to be parsed.
The parser generator fills prediction table, leaving the
  rest to the parsing algorithm in~\cref{algorithm:ll-parser}
The prediction table construction is depicted in~\cref{algorithm:generation}

\begin{algorithm}
  \caption{\label{algorithm:generation}
  An algorithm for filling the prediction table~$\Function predict(A,b)$ for some
    grammar~$G =⟨Σ,Ξ,P⟩$ and an end-of-input symbol~$\$∉Σ$,
    and where~$A∈Ξ$ and~$b∈Σ∪❴\$❵$.}
    \begin{algorithmic}
    \FOR[for each grammar rule]{$r∈P$}
      \LET{$A ::=α$}{$r$}\COMMENT{break~$r$ into its RHS ＆ LHS}
      \FOR[for each terminal in~$\Function First(α)$]{$t∈\Function First(α)$}
        \IF[is there a conflict?]{$\Function predict(A,t)≠⊥$}
          \STATE{\textsc{Error}}\COMMENT{grammar is not LL(1)}
        \FI
        \STATE{$\Function predict(A,t) = r$} \COMMENT{set prediction to rule~$r$}
      \ENDFOR
      \IF[$α$ might derive to~$ε$]{$\Function Nullable(α)$}
        \FOR[for each terminal in~$\Function Follow(A)$]{$t∈\Function Follow(A)$}
          \IF[is there a conflict?]{$\Function predict(A,t)≠⊥$}
            \STATE{\textsc{Error}}\COMMENT{grammar is not LL(1)}
          \FI
          \STATE{$\Function predict(A,t) = r$} \COMMENT{set prediction to rule~$r$}
        \ENDFOR
      \FI
    \ENDFOR
  \end{algorithmic}
\end{algorithm}


