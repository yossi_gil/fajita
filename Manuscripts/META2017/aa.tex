%! TEX root = 00.tex

Any \NonCitingUse{Java} compiler is also a simple pass-fail computing automaton
in this sense: given a \NonCitingUse{Java} program, the automaton passes if the
program type checks, and fails otherwise.
Studies~\cite{Gil:Levy:2016,Grigore:17} of the computational class of this device
were motivated by the practical problem of automatically generating fluent APIs
in \Java from DSLs which are more interesting than a finite automaton.

If this was possible, then instead of the non type safe and error prone regular expressions
literals such as \cc{"([a-z]+B*)?"}, search and replace and pattern matching
libraries can make use of fluent API specification such as
\begin{quote}
  \cc{optional().from('a').to('z')} …
\end{quote}
Previous results were theoretical---the size of the produced \Java library was
exponential in the size of the DSDL's specification.

Here, we give the first practical algorithm for this problem. Given an LL
specification of a context free language, the algorithm (and the demo
implementation, \Fajita), construct a fluent API \Java implementation,
whose size blowup is quadratic in the worst case, but, as can be inferred
be gained and folk-lore and experience in LL parser generators, often closer to
being linear.

\subsection{Background: Compile Time Computation}
The \CC compiler is Turing complete in the this sense: for every Turing
machine~$M$ (written in such a way that its input tape is encoded as part
of~$M$), there exists as \CC program,~$P_M$, such that the \emph{compilation}
of~$T$ halts, if and only if, machine~$M$ halts (see,
e.g,~\cite{Gutterman:2003}). In other words, there are many \CC programs which
will send any \CC compiler into an infinite loop.

An important result of R.~Grigore~\cite{Grigore:17} disproved the belief that
type erasure and the absence of specialized generics makes \Java's type system
decidable. The Grigore construction however describing an elegant translation
of the steps of a Turing machine into steps of the \Java type inference engine.

Results of this sort are of great interest to theoreticians, designers of
programming languages, and compiler writers. But why should a programmer, a
client of these languages, care about what can be computed, and what can not be
computed by coercing the type system and the type checker of a certain
programming language to do abstract computations it was never meant to carry
out?

Evidently, the fact that compilation of \CC is equivalent to the halting
problem, did not stop compiler vendors from producing compilers. And, the
recent discovery that type inference in \Java may never halt, seem to have had
no impact on software development in \Java, nor on the production of \Java
compilers.

Indeed, practitioners do not perceive the fact that the type system of their
programming language is Turing complete as a major limitation: Programs which
bring about undecidability issues are extremely unlikely to occur in programs
written for usual programming tasks, do not come close to testing the compiler
limitations. And, there are engineering workarounds to the halting problem,
such as limiting the depth of recursion depth to some large number, or applying
generous timeout values.

Conversely, practitioners may enjoy their compiler is Turing complete, since
this means that any computation can be carried out at compile time. An
important example is \CC in which there are general purpose libraries for
compile time computation~\cite{Abrahams:Gurtovoy:04}. There are also famous
applications of \CC compile time computation for producing SQL
queries~\cite{Gil:Lenz:07a}, symbolic computation of the derivative function,
as required, e.g., by the Newton-Raphson method~\cite{Gil:Gutterman:98}, and a
rich theory of expression templates due to Veldhuizen~\cite{Veldhuizen:95a}.

\subsection{Compile Time Computation in \Java and Fluent APIs}
Unfortunately, none of these libraries made its way to the \Java world. The
evident effort made by \Java designers to simplify the language, and the
prudent manner in which its generics were
designed~\cite{Bracha:Odersky:Stoutamire:Wadler:98}, made compile time
computation in \Java very difficult. As it turns out, Grigore's reduction (just
like Gutterman's) does not provide clues to efficient and useful compile time
computation. Grigore demonstrated his work with a general purpose parser, alas,
the space (in terms of generated class) and time (of compilation) are
prohibitively large even for minuscule inputs.

The question that intrigues this research is whether compile time computation
can bring benefit to \Java programmers just as it does for \CC programmers. We
concentrate on the problem of automatically generating fluent API
libraries~\cite{VanDeursen:Klint:2000,Hudak:1997,Fowler:2010}, e.g.,
\begin{quote}
  \javaInput[]{../Fragments/jOOQ-mini.java.fragment}
\end{quote}
---a use case of jOOQ\urlref{http://www.jooq.org}, a framework for writing
SQL like code in \Java, much like LINQ project~\cite{Meijer:Beckman:Bierman:06}
in the context of \CSharp, and
\begin{quote}
\javaInput[]{../Fragments/camel-apache.java.fragment}
\end{quote}
---a use case of Apache Camel~\cite{Ibsen:Anstey:10}, open-source integration
framework.

There are many advantages for implementing a DSL as a fluent API library.
With fluent APIs programmers can use the IDE's auto-completion feature to
quickly write programs that use the API correctly, even without understanding
the exact rules that govern correct use. Moreover, programs that use a well
designed fluent API, are arguably more readable than those that use plain,
non-fluent API which often relies on auxiliary variables.

However, fluent API is easy to use but hard to realize, and even harder to
maintain. The difficulty increases with the level in the Chomsky hierarchy of
the DSL. There is an efficient algorithm for automatically producing a \Java
implementation for a given DSL belongs to the class of regular language in the
Chomsky hierarchy. This algorithm translates the underlying deterministic
automaton to classes, and transitions to methods of these classes, and
can therefore usable for producing code for any object bases programming
language.

The problem of generating fluent APIs is intimately related to the problem of
language recognition: To be able to automatically create a fluent API for a
class of languages~$𝓛$ the computational power of the type system should be
able to recognize languages in~$𝓛$. To be able to automatically generate
a fluent API for a specification of a language in~$𝓛$, the type system
of the underlying language (\Java) must be able to recognize the family~$𝓛$.

Unfortunately, writing libraries that support fluent API is not an easy task.
A fundamental problem is then that of automatically generating from the fluent
API specification the necessary classes and methods to support this
specification.

\subsection{Contribution}

Gil and Levy~\cite{Gil:Levy:2016} showed that the type system of \Java includes
all deterministic context free languages. Their construction gives rise to a
recognizer of such languages which works in linear time. However, this
theoretical result takes a toll of exponential blowup in size: The construction
emulates a deterministic pushdown automaton whose size parameters,~$g$ (number
of stack symbols), and,~$q$ (number of internal states), are polynomial in the
size of the input grammar. This automaton is then emulated by a weaker
automaton, with as many as
\begin{equation}
  \label{equation:exponential}
  O\left(g^{O(1)}\left(q²g^{O(1)}\right)^{qg^{O(1)}}\right)
\end{equation}
stack symbols, each of which must be emulated by a distinct \Java class.

A theorem by Grigore shows that with covariant subtyping, the \Java type
systems becomes Turing complete, and therefore can ``recognize'' all recursively
enumerable languages.

Grigore also provides a more efficient construction, which can recognize
\emph{any} context free language, but involves the cubic time of the
CYK~\cite{Cocke:1969,Earley:1970,Younger:1967}
algorithm, that is considered too inefficient to be used even by ordinary parsers;
The implementation in the \Java type system contributes another factor of polynomial size in the
grammar.

The contribution of this paper is a linear time algorithm for recognizing
LL(1) languages and adding at the worst quadratically sized code. More concretely,
recall that an LL(1) grammar $G$ of size $n$ has a constant $k=k(G)$ which
describes the ``span out'' or diversity of the grammar. It is the folk-lore 
of LL parser generators that in typical languages $k≪ n$, often as small as 4 or 3.

\Cref{table:results} compares the current contribution with previous results.

\begin{table*}
  \caption{Algorithms for compile time recognition of words of length~$m$ of a language of class~$𝓛$ whose grammar size is~$m$}
  \label{table:results}
  \scriptsize
  \centering
  \rowcolors{2}{}{olive!20}
  \begin{tabularx}\linewidth{@{}l@{} c@{} *4l X}
    \toprule
    &
    \multicolumn1r{\bfseries Impl.} & \hfill \bfseries~$𝓛$ \hfill & \hfill \bfseries Lang.\@ Property \hfill &\hfill \bfseries Time \hfill & \bfseries Space & \bfseries Method ⏎
    \midrule
--2017 (folk-lore) & ✓ & Regular & \cc{\kk{class} C{$…$}} & $O(m)$ & $O(n)$ & direct translation of the DFA ⏎
    Gil ＆ Levy 2016~\cite{Gil:Levy:2016} & ✗ & LR($κ$),~$κ>0$ & \cc{\kk{class} C<T \kk{extends} S>{$…$}} & $O(m)$ & $O(nⁿ)$ (see \cref{equation:exponential}) & simulation of a jump automaton ⏎
    Grigore 2017~\cite{Grigore:17} & ✓ & Recursively enumerable
                                   & \cc{\kk{class} C<T \kk{super} S>{$…$}}
                                   & exponential(?)
                                   & exponential(?)
                                   & simulation of Turing machine ⏎
    Grigore 2017~\cite{Grigore:17} & ✓ & Context free
                                   & \cc{\kk{class} C<T \kk{super} S>{$…$}}
                                   & $O(m³n^{O(1)})$
                                   & $n^{O(1)}$ 
                                   & frugal implementation CYK~\cite{Cocke:1969,Earley:1970,Younger:1967} on 
                                  the \Java type system ⏎
    Current & ✓ & LL(1) & \cc{\kk{class} C<T{$…$}>} & $O(m)$ & $O(nk)$ & emulating a jump automaton ⏎
    \bottomrule
  \end{tabularx}
\end{table*}

The first row of the table describes the familiar technique of constructing
fluent APIs for regular languages. To do so, construct the DFA of the language,
and then follow this recipe.

\begin{enumerate}
  \item A state~$q$ in the automaton is encoded as an \kk{interface}.%
        †{%
          The name of this interface is arbitrary; all that it is required is
          that names assigned to distinct states are distinct. For readability
          sake, there is often an attempt to choose a name which makes a close
          approximation of the name of~$q$, within the limits of the language's
          syntax and Unicode vocabulary.
        }
  \item If there is an arc labeled~$ℓ$ leading from~$qᵢ$ to~$qⱼ$,
        then \kk{interface}~$qᵢ$ defines a
        method whose return type is \kk{interface}~$qⱼ$.%
        †{%
          Again, the name of this method is often selected as an approximation
          of the name of~$ℓ$.
        }
  \item The initial state~$q₀$ is made special in being the only type from
        which a fluent API call chain can start.%
        †{%
          This is achieved by generating a class~$c$ with \cc{public}
          constructor that \kk{implements} the \kk{interface} encoding
          type~$q₀$. If no other interfaces do not have a similar~$c$, then
          clients can only start a fluent API chain by creating an instance
          of~$c$, which is
          effectively the state~$q₀$.
        }
\end{enumerate}

There are several tools that follow this recipe. For example, the code in
\cref{figure:fluflu} uses annotation to instruct
fluflu\urlref{https://github.com/verhas/fluflu} to generate the fluent API for
the regular language~$a^*$.

\begin{figure}[H]
  \caption{\label{figure:fluflu}
  A fluflu specification of the DFA recognizing the regular language~$a^*$}
  \javaInput{fluflu.example.listing}
\end{figure}

Note that the DFA of the figure has a single, accepting state, with a single
self transition, labeled with terminal~$a$, whereby realizing the regular
language~$L=a^*$.

Non-regular languages break the above recipe, since the number of states is
unbounded---such languages require (at least) a pushdown automaton, and the
number of states in this is unbounded.

The observation that the states of the automaton can be represented by \Java
generics made it possible (second row of \cref{table:results}) to generate
parser for regular languages. Relying only on simple generics, i.e., upper
bound parameters, guaranteed linear time in left-to-right parsing. However, the
nature of the parsing method made it impossible to deal with non-deterministic
languages, which cannot be deterministically recognized with left-to-right
parsing.

The next observation, that lower bounds on generic parameters, and the
contra-variant type checking it requires, allow for multiple passes on the
input, and an emulation of a Turing machine, made it possible (third row of
\cref{table:results}) to deal with the entire family of context free
languages.

The current result (last row of the table) requires only simple generics, and
hence only linear processing time. The observation that made it possible is
that jump-pushdown automata can be emulated by simple generics. (Recall that
jump pushdown automata are pushdown automata whose stack supports constant time
operations of popping many stack elements, until an element of a given value is
found on the stack.)

Thus, the current contribution brings the complexity metrics of recognizing
context free into practical feasibility with linear time processing and
polynomial space. In fact, even the quadratic worst case space requirements is
in practice close to linear: Our algorithm emulates the classical LL(1) parsing
generator. The number of states this algorithm generates is indeed~$O(n²)$ in
the worst case, but experience with LL(1) grammars shows that is typically much
smaller, since most entries in the transition table are empty. In our
construction, the number of methods in a generated class that represent a
parser state is precisely the number of symbols that may appear on the input at
this state. In typical grammars these symbols are much fewer
than the full
vocabulary of input symbols.

Our implementation of the algorithm is available in the form of the
\Fajita†{\itshape \textbf Fluent \textbf API for \textsc{\textbf Java} (\textbf
Inspired by the \textbf Theory of \textbf Automata)} tool: a \Java library,
which, given an LL(1) grammar specification of a language, generates the
classes and methods that realize the fluent API for this language. However,
at this time, the practicality of \Fajita is limited, since it cannot yet
generate the abstract syntax tree of the given language. As explained below,
more engineering work is required to integrate AST generation into \Fajita.

\subsection{Other Related Work}
The challenges of \Java generic programming were highlighted by Garcia et
al.~\cite{Garcia:Jarvi:Lumsdaine:Siek:Willcock:03} research on the expressive
power of generics in half a dozen major programming languages, Indeed, unlike
\CC~\cite{Austern:98,Musser:Stepanov:1989, Backhouse:Jansson:1999,
Dehnert:Stepanov:2000,Gil:Gutterman:98,Abrahams:Gurtovoy:04}, the literature on
meta-programming with \Java generics is minimal.

Suggestions for semi-automatic generation of fluent APIs in \Java can be found
e.g., in the work of Bodden~\cite{Bodden:14} and on numerous locations in the
web. However, not too many of these matured into an actual general purpose
implementation, and non of

Fluent APIs are related to the topic of \emph{type-states}, on which there is a
large body of research on \emph{type-states} (see e.g., these review
articles~\cite{Aldrich:Sunshine:2009,Bierhoff:Aldrich:2005}).
Informally, an object that belongs to a certain type the object has
type-states, if not all methods defined in this object's class are applicable
to the object in all states it may be in.

File object is the classical example: It can be in one of two states: ‟open” or
‟closed”. Invoking a \cc{read()} method on the object is only permitted when
the file is in an ‟open” state. In addition, method \cc{open()} (respectively
\cc{close()}) can only be applied if the object is in the ‟closed”
(respectively, ‟open”) state.

Beckman~\cite{Beckman:11} estimates that about~$7.2％$ of \Java classes define
protocols definable in terms of type-states. This non-negligible prevalence
raise two challenges:
\begin{enumerate}
  \item \emph{\textbf{Identification.}} Frequently, type-state receive little
    or no mention at all in the documentation. The challenge is in identifying
    the implicit type state in existing code.
        \par
    Specifically, given an implementation of a class (or more generally of a
    software framework), \emph{determine} which sequences of method calls are
    valid and which violate the type state requirement presumed by the
    implementation.

  \item \emph{\textbf{Maintenance and Enforcement.}} Having
    identified the type-states, the challenge is in automatically flagging out
    illegal sequence of calls that does not conform with the type-state.
        \par
    Part of this challenge is maintenance of these automatic flagging
    mechanisms as the type-state specification of the API evolves.
\end{enumerate}

The result presented here is a step forward towards meeting these challenges.
Ideally, object protocols are specified by a language. The current result shows
that it is possible for LL(1) languages, as required e.g., for defining nested
transactions in database, or as is the case in the \textsc{Builder} design pattern.
