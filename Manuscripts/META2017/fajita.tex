\Fajita is a general-purpose fluent API generator, made for LL(1) grammars.
With \Fajita, one can generate a type-safe fluent API for \Java from the
grammar specification. The input of \Fajita is a BNF or a context-free grammar
that defines the API's syntax. Incidentally, this grammar specification is
itself made using fluent API.

Consider for example, the BNF specification of a regular expression language in
\cref{figure:re-BNF}.

\begin{figure}[H]
  \caption{\label{figure:re-BNF}
    An LL(1) BNF grammar for a regular expression language
  }
  \begin{adjustbox}{}
    \begin{Grammar}
      \begin{aligned}
        \input{re.grammar.listing}
      \end{aligned}
    \end{Grammar}
  \end{adjustbox}
\end{figure}

In the figure nonterminals are capital-case words, separated by an
underscore~(‟\texttt{\_}”), e.g.,~{$⟨SIMPLE\_RE⟩$}, while terminals are
camelCase, e.g.,{$atMost$}.

The grammar employs right-recursion to state that a regular
expression~({$⟨RE⟩$}) is a list of simple regular expressions
({$⟨SIMPLE\_RE⟩$}). A simple regular expression in turn is either a string, a
set of characters, a range of characters, one of a certain, pre-defined class
of characters, or a wildcard (known collectively as $⟨NO\_DUPL\_RE⟩$), followed
by a quantifier that says how many times this simple regular expression occurs.
Alternatively, a simple regular expression is made a by the
keyword~$zeroOrMore$ or~$moreThanZero$, followed by parenthesis wrapping a
{$⟨NO\_DUPL\_RE⟩$}.

The following are two examples of regular expressions confirming to the
language defined in \cref{figure:re-BNF}:
\begin{quote}
  \javaInput[]{re-example.fragment}
\end{quote}

To convert this grammar specification to the fluent API specification of
BNF grammars that \Fajita expects, it is required first to define a list
of all terminals of the language. This is done in the \kk{enum} definition in
\cref{figure:re-terminals}.

\begin{figure}[H]
  \caption{A \Java definition of terminals of the grammar of \cref{figure:re-BNF}}
  \label{figure:re-terminals}
  \javaInput{re.terminals.listing}
\end{figure}

For technical reasons, the \kk{enum} type in the figure must implement
interface \cc{Terminal}.  Each of the \kk{enum} values figure will be converted
by \Fajita to a name of a method. For a token~$t$, there would be a method
named~$t$ in each class that represents a state in which token~$t$ is
recognized. Moreover, if the input may begin with the token~$t$, then \Fajita
will define a \kk{static} method of an appropriate class, so that with the
correct \kk{import static} directive, a fluent API call chain may begin
with~$t$ with no receiver.

Typically these methods do not take any parameters (other than the implicit
\kk{this} parameter to non-\kk{static} methods. However, this is not always the
case.  In the grammar of \cref{figure:re-BNF} tokens \cc{atLeast} and
\cc{atMost} are qualified by an integer. The \Fajita grammar definition will
instruct it to generate the corresponding method with a single \kk{int}
parameter.

\Fajita supports also methods whose argument is any nonterminal symbol,
including the grammar's start symbol. In our example, methods \cc{oneOrMore}
and \cc{moreThanZero} shall take a parameter of type \cc{NO\_DUPL\_RE}.

In general, the grammar's start symbol, just as any other nonterminal symbol in
the grammar must be defined in an appropriate \kk{enum}.
\Cref{figure:re-nonterminals} shows the \kk{enum} definition for the grammar in
\cref{figure:re-BNF}.

\begin{figure}[H]
  \caption{A \Java definition of the nonterminals of the grammar of \cref{figure:re-BNF}}
  \label{figure:re-nonterminals}
  \javaInput{re.nonterminals.listing}
\end{figure}

Again, for technical reasons, this \kk{enum} must implement
interface \cc{NonTerminal}. Its name however is arbitrary. Each
of the  values in this \kk{enum} will be translated by \Fajita to a type.

(An alternative design would have used strings rather than values of enumerated
type to defined terminals and nonterminals. Strings however are not type-safe
and therefore error-prone.  In addition, the quotation marks are syntactic
baggage that degrade readability: the reader should largely ignore these.)

With the above definitions of the set of terminals and nonterminals, we are
ready to use a fluent API call chain to \Fajita to generate classes and methods
that realize the fluent API of the grammar itself. The \Fajita specification
of the grammar in \cref{figure:re-BNF} is found in \cref{figure:re-fajita}.

\begin{figure}[H]
  \caption{The \Fajita fluent API call chain defining the grammar of
    \cref{figure:re-BNF}, using the terminals of \cref{figure:re-terminals} and
    the nonterminals of \cref{figure:re-nonterminals}.
  }
  \label{figure:re-fajita}
  \javaInput{re.fajita.listing}
\end{figure}

The grammar specification in the figure is made by a fluent API call chain,
where \cc{derives} denotes the symbol that separates the head of a derivation
rule from its body, \cc{and} denotes concatenation, \cc{or} denotes the start
of an alternative body of a rule, \cc{start} denotes the start symbol,
\cc{orNone} denotes and $ε$-derivation, etc.

In writing \cc{or(atMost, \kk{int}.\kk{class})} the fluent API designer
specifies that method \cc{atMost}, when occurring at this position in the
grammar must take a single parameter of type \kk{int}, i.e., that occurrence of
token $atMost$ in this context, must be qualified by an integer.  Also, in
writing \cc{to(chars, Fajita.ellipsis(char.class))} the designer specifies that
method \cc{chars} may take a variable number of arguments, each of type
\kk{char}.

More interestingly, the specification \cc{or(zeroOrMore, NO\_DUPL\_RE)} says
that the method \cc{zeroOrMore} takes a parameter of type \cc{NO\_DUPL\_RE}.
This means that \Fajita generates also a distinct grammar for the start symbol
\cc{NO\_DUPL\_RE}. The parameter to function \cc{zeroOrMore} can be a fluent
API call which generates and object of type \cc{NO\_DUPL\_RE}.

The final \cc{go} call instructs \Fajita to go about its code generation. When
this happens, the fluent API client can rely on the IDE's auto-completion
feature to help compose correct specification of a regular expression, as show
in this screen capture:
\begin{quote}
\begin{adjustbox}{W=0.0}
  \includegraphics{../Figures/auto-suggest.png}
\end{adjustbox}
\end{quote}
  
Conversely, incorrect specification of a regular expression are marked
as compilation error, as shown in these two examples:

\begin{quote}
\begin{adjustbox}{W=0.9}
    \includegraphics{../Figures/compilation-error.png}
\end{adjustbox}
\end{quote}

We conclude this brief description of \Fajita with an excerpt of the code it
generates for the specification in \cref{figure:re-fajita}. As can be seen 
in the screen capture of \cref{figure:generated} \Fajita makes a substantial
use of generics.

Also, evident in the figure is the fact that \Fajita does not yet generate method bodies.
Currently, these are to be written by the API designer in such a fashion
that the AST is created, either as parsing progresses, or, alternatively,
when it ends.

This ``end of fluent API chain parsing'' (which is done by \Fajita itself when
it parses its input), is done by collecting method names (which are just token
names) as they occur into an input list.  The input list, which necessarily
forms a correct word of the language, can then and then repeating the parsing
process at runtime, with the final call to the API, to generate the AST of the
desired language. 

\begin{figure}[H]
  \caption{An excerpt of the code generated by \Fajita's in response to the 
    specification of \cref{figure:re-fajita}
  }
   \label{figure:generated}
  \begin{adjustbox}{}
    \includegraphics{../Figures/generated-code-example.png}
  \end{adjustbox}
\end{figure}

