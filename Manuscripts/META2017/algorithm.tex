%! TEX root = 00.tex

This section sketches the algorithm for converting an LL grammar into \RLLp, 
a parsing automaton equipped with a stack
that has the unique quality of spending constant time on each read input.
This realtime property is crucial to the implementation language recognizers
with \Java types implementation.


When this is achieved, all that remains is the ‟compilation”' of the \RLLp into
\Java types. The challenging part of this translation is dealing
with the ‟JSM”, a data structure on which the \RLLp relies.

The algorithm itself is involved, and cannot be presented in full here. The
many details are also difficult to verify by manual inspection. Correctness
is demonstrated by the \Fajita implementation.


For intuition, recall that the JSM is used, e.g., for efficient management of the
runtime environment in programming languages with dynamic scoping, i.e.,
languages such as \Lisp, in which name resolution is with respect to previous
bindings made on the run time stack (disregarding the usual scoping and nesting
rules in languages such as \Pascal).

Also, recall that the JSM is intimately related to ‟\emph{Jump Deterministic
PushDown Automata}" (JDPDA), a theoretical model used in the study of automata
and formal languages (see e.g.,~\cite{Linna:79,Courcelle:77}).  And,
Incidentally, this JDPS is the one used by Gil and Levy~\cite{Gil:Levy:2016},
in their proof that the \Java type checker can recognize DCFG languages. Using
their construction, the translation to \Java is rather mechanical.

The main intuition behind the algorithm we present is that the realtime
property is achieved by careful examination of the algorithm presented in the
previous
section, and applying pre-computation and storing appropriate abstract state on
the JSM.

\subsection{The Realtime LL Parser}
\label{section:realtime}
The \emph{\textbf Realtime \textbf Left-to-right \textbf Leftmost-derivation
  Parser} (\RLLp), is a variant of the famous LL(1) parser~\cite{Lewis:66}. The
adjective realtime is to claim that:

\begin{itemize}
  \item an \RLLp examines its input only after consuming it, and,
  \item an \RLLp conducts at most one (potentially extended) stack operation in
        each step.
\end{itemize}

An extended stack operation is either a~$\Function push(α)$ of a string of
symbols, or a long~$\Function jump(·)$ into the stack position denoted
by its argument, involving an unbounded number of~$\Function pop()$ operation.

The stack symbols of an \RLLp are \emph{items}, where an item is pair of a
grammar rule and a ‟\emph{dot}”, written as
\[
  A ::= Y₀…Y_{i-1}·Yᵢ…Yₖ
\]
Formally, the dot is an integer, ranging from 0 to the length of the right-hand
side of the rule. But it is better to think of it as a notation for the prefix
of this right-hand side.

An item represents these precise moments in the analysis process of the
input in which:
\begin{itemize}
  \item all symbols included in this prefix have been successfully parsed, and,
  \item all symbols that lie after this prefix, are awaiting their turn
        to be parsed.
\end{itemize}

Items can be thought of as a generalization of the stack symbols of an \LLp
(recall that these are the grammar's terminals and nonterminals). \LLp stack
symbols are markers of the next symbol to be read or parsed. \RLLp stack
symbols store this information as well: The next symbol to be read or parsed,
is simply the symbol that follows that dot. The generalization is in adding to
this symbol the context of containing rule and the point of derivation within
it.

Just like the \LLp, the \RLLp is a stack automaton equipped with a prediction
table, whose next action is a function (realized in the prediction table) of
the next input symbol and the item present at the stack's top.

The \RLLp is initialized with the stack containing an item denoting the
degenerate prefix of a rule for deriving the start symbol. In case there are
more than one such rule, the automaton selects the rule dictated by the first
input token. (This rule is uniquely determined since the grammar is LL.)

After this initialization, the \RLLp proceeds following the instructions
in~\cref{algorithm:rll-parser}.

\begin{algorithm}
  \caption{\label{algorithm:rll-parser}
    A high level sketch of the iterative step of an \RLLp
  }
  \begin{algorithmic}[1]
    \LET{$[X ::=α·Yβ]$}{\Function pop()}\COMMENT{retrieve parsing state}
    \LET{$t$}{$\Function next()$}\COMMENT{examine input once per iteration}
    \IF[was rule fully parsed?]{$|Yβ|=0$}
      \IF{$X$ is not the start symbol}
         \STATE{$\Function jump(t)$}\COMMENT{pop this, potentially other items}
         \CONTINUE\COMMENT{restart, popping a new item}
      \FI
      \IF[$X$ must be the start symbol]{$t=\$$}
        \STATE{\textsc{Accept}}\COMMENT{start symbol fully parsed}
      \ELSE[start symbol parsed, but not all input consumed]
        \STATE{\textsc{Reject}}\COMMENT{\RLLp halts in error}
      \FI % Not anticipated EOF
    \FI % Was rule fully parsed?
    \IF[$Y$ is a terminal]{$Y∈Σ$}
      \IF[read terminal is not anticipated~$Y$]{$Y≠t$}
        \STATE{\textsc{Reject}}\COMMENT{\RLLp halts in error}
        \ELSE[anticipated terminal found on input, proceed]
      \STATE{$\Function push(X::=αY·β)$}\COMMENT{push item with dot advanced}
      \CONTINUE\COMMENT{restart, popping this item, and parsing~$β$}
      \FI % Not anticipated input token
    \FI % Y is a terminals
    \STATE{\textbf{assert}~$Y∈Ξ$}\COMMENT{$Y$ must be a nonterminal}
    \LET{$a$}{$\Function Δ(Y, t)$}\COMMENT{$Δ(Y,t)$ says what to do with~$Y$}
    \IF[input~$t$ was unanticipated]{$a=⊥$}
      \STATE{\textsc{Reject}}\COMMENT{\RLLp halts in error}
    \FI
    \IF[a string of~$ℓ$ items to push]{$a=I₁,…,I_ℓ$}
    \STATE{$\Function push(I₁, …, I_ℓ)$} \COMMENT{note, this is a single push operation}
      \CONTINUE\COMMENT{restart with a somewhat deeper stack}
    \FI
    \STATE{\textbf{assert}~$a$ represents a jump} \COMMENT{$t$ indicates that~$Yβ\stackrel*⇒ε$}
    \STATE{$\Function jump(t)$}\COMMENT{pop this, potentially other items}
  \end{algorithmic}
\end{algorithm}

Comparing \cref{algorithm:rll-parser} with the LL-parsing algorithm
(\cref{algorithm:ll-parser}), we see that they both begin with popping a stack
symbol. More similarities are apparent, after observing that the next
symbol to read or parse, denoted by~$X$ in \cref{algorithm:ll-parser} is obtained
by extracting the symbol~$Y$ that follows the dot in the item~$X$ in
\cref{algorithm:rll-parser}.

In some ways, our \RLLp emulates an \LLp except that it attaches to each symbol
the rule in which it was found and the location within this rule.
(Thus, a push of single item is equivalent to the loop of push operations in
line 20 in \cref{algorithm:ll-parser}).

Function~$Δ(·,·)$, the transition function of an \RLLp is also a bit more
general, and may command the algorithm to push a sequence of stack symbols
(items), or carry out a jump into the stack.

\subsection{The Jump Stack Map Structure}
We still need to explain how the jump operations of \RLLp (lines~5
and~25 in \cref{algorithm:rll-parser}) are implemented. For this purpose, we
construct here the \emph{\textbf Jump \textbf Stack \textbf Map} (JSM).

Let~$S₀$ be the implicit stack of items used by the \RLLp, and suppose that
this stack is implemented as a singly linked list of nodes of an appropriate
type.

The top item of a stack (and in particular~$S₀$) is represented by a pointer,
called the ‟\emph{top pointer}”. This pointer can be used for pushing or
popping from the list. It can also be used for pushing into the stack a
pre-made string of items as done in line 22 of the \RLLp algorithm.

Storing pointers into designated items that lie deeper in the stack makes it
possible to make a direct jump into these items. We call these pointers
‟\emph{jump pointers}” and use the letter~$J$ to denote the type of these.

Let us now build on top of~$S₀$ an abstract data type~$D$, which can thought of
as a stack of dictionaries, supporting ordinary push and pop operations:
\[
  D=dₙ⋯d₁\$
\]
where~$dᵢ$,~$i=1…n$ is a (partial) map of the type~$Σ↛J$ ($Σ$ being our
alphabet,~$dₙ$ being the top of the stack and~$d₁$ the deepest item in it).

Sending query~$\Function get(t)$ to~$D$,~$t∈Σ$ returns the~$J$ value
associated with~$t$ in the top most (maximal~$i$) dictionary~$dᵢ$, which
satisfies~$t∈dᵢ$. The query returns~$⊥$ if~$t$ is not found in any of
the~$dᵢ$s.

There is an efficient implementation of~$D$ in which~$\Function get(t)$
is~$O(1)$ time, regardless of the depth at which~$t$ is found in~$D$, while
keeping updates of the form~$\Function push(d)$ or~$d←\Function pop()$
in~$O(|d|)$ time ($|d|$ being the size of the partial function).

In fact, since~$D$ has the same semantics as that of the stack of binding in
dynamically scoped language†{a dictionary~$dᵢ$ is a collection of
  names and their binding to nameable entities defined in the scope, which hide
the definitions made in containing scopes} definitions (such as
\TeX~\cite{TeX:79}), we can rely on the classical method~\cite{Schoe:95} for
implementing these

\begin{enumerate}
  \item Maintain a hash table~$H$ mapping each~$t∈Σ$ to a \emph{stack}~$s(t)$
        of values of type~$J$. A search for key~$t\inΣ$, then returns in~$O(1)$
        time the value at the top of~$s(t)$ or~$⊥$ if stack~$s(t)$ is empty.

  \item The call~$\Function push(d)$,~$|d|=m$ is implemented in~$O(m)$ time. Let
        \[
          d=❴(t₁,j₁),…,(tₘ,jₘ)❵.
        \]
        Then iterate over the pairs~$(tᵢ, pᵢ)$,~$i=1,…,m$,
        pushing~$pᵢ$ to stack~$s(tᵢ)$.

        The call~$\Function push(d)$ terminates by pushing~$d$ itself
        (represented, say, as linked list) as a single item into an auxiliary stack,
        to be denoted~$S₁$.

  \item Thus, abstract data type~$D$ is implemented as the pair~$⟨S₁,H⟩$.

        When~$D$ pops a map~$d$, it uses~$S₁$ to locate~$d$, and just before
        returning it, the implementation of~$D$ uses this~$d$ to pop an element
        from the set~$s(dᵢ)$ of stacks in~$H$,
        \[
          s(dᵢ) = ❴s(t) \;|\; t∈dᵢ❵.
        \]
\end{enumerate}

Let us now generalize~$D$ so that it also supports jumps into it. To do so, let
clients of~$D$ store pointers to designated dictionaries in the stack~$S₁$.

Upon jumping to a deep dictionary~$dᵢ$, our generalized~$D$ must be able to do
a constant time jumps in all stacks in~$s(dᵢ)$.

For realizing constant time jump we equip each~$dᵢ$ in the stack~$S₁$ with the
correct jump pointers into these stacks. The value of these jump pointers is
set as the top pointers of stacks~$s(t)$ at the time~$dᵢ$ was pushed into~$D$.

A jump into~$D$, yielding a dictionary of size~$m$ can now be implemented
in~$O(m)$ time: The jump pointer into stack~$S₁$ yields a dictionary~$d$ stored
in it. In~$d$ we find~$m$ jump pointers into the~$m$ stacks~$s(d)$ in~$H$,
carrying out the these~$m$ jumps, concludes the jump operation on~$D$.

The final step in constructing the JSM is by coordinating
stacks~$S₀$ and~$S₁$:
\begin{itemize}
  \item A push operation on stack~$S₀$ is required to push, a (potentially
        empty) dictionary into the stack~$S₁$.
  \item A pop operation (the first command at every step of~\cref{algorithm:rll-parser})
        on stack~$S₀$ forces a pop operation of stack~$S₁$.
  \item Augment items in stack~$S₀$ to include also the jump pointer to
        the dictionary it pushed into~$S₁$.
  \item Let a jump into stack~$S₀$ also
        force a jump into the stack~$S₁$.
        (As expected, a jump into an item~$i$ means also a jump
        in stack~$S₁$ to dictionary~$dᵢ$.)
\end{itemize}
Thus, the linearly sized JSM data structure keeps its two
duties: supporting jumps into the items stack,
and maintaining a current map of the jumps to carry out at each item.
This map is updated with every push, which might
override (or add) entries to it.
Most importantly, this map is correctly restored
in the process of long jumps into the items stack.

In a way, the JSM is a data structure generalizing the symbol table
of a programming languages with lexical binding.
The jump operation in the JSM generalizes a ‟goto” operation to
label on the stack.

\subsection{Push After Jump}
\label{subsection:push-after-jump}

Before the algorithm for coordinating stacks~$S₀$ and~$S₁$
is shown, another problem regarding the jump operation needs
to be discussed.
This problem combines both~$k'$ and~$k^*$ problems.

Breaking down the steps, a non-realtime \LLp would perform three steps before
reading the input symbol \cc{begin}:

\begin{description}

  \item[Popping] since the item at the top of the stack was fully parsed, and
        since \cc{begin} is in~$\Function Follow(\<Constant>)$, the \LLp would pop
        all fully parsed rules, without consuming the input symbol.

  \item[Advancing] upon finishing the parse of \<Definitions> in the derivation
        of \<Program>, the \LLp would advance to parse \<Body>.

  \item[Consuming] since \<Body>'s only rule begins with the terminal
        \cc{begin}, the input symbol will finally be matched.

\end{description}

The first step encapsulates the~$k^*$ factor, while the other
two encapsulate the~$k'$ factor.

The \RLLp needs to perform all three steps \emph{together}, in a constant
amount of work, meaning it needs to jump (completing the first step), push the
advanced item of \<Definitions> (second step) and lastly push \<Body>'s item.

For that cause the \RLLp relies on the JSM, supporting constant time jump operations, and
solving the~$k^*$ factor problems. The \RLLp also relies on the~$\Function
Consolidate(·,·)$ function (introduced later at
\cref{subsection:consolidate}) that solves the problems that rise with~$k'$ factor.

But the two problems combine as the \RLLp is required to \emph{Push after Jump} in realtime.

The problem is solved by elaborating the type of previously mentioned~$J$ (the
jump pointer type) to hold also information regarding the push that will occur
after each jump.

\subsection{The Jumps Dictionary}
How should the JSM be used by the algorithm that generates a specific \RLLp?

Jump operations are all at the responsibility of the JSM, which maintains the
information required to support these. But, in order to be able to do this,
the generator must provide the \RLLp with the contents dictionary~$D$ that
should be pushed to~$S₁$, the~$dᵢ$s, together with the push to~$S₀$ (lines~15
and~22 in \cref{algorithm:rll-parser}).

In particular, the \RLLp generator must compute for each
item~$i∈I$, the dictionary~$\Function Jumps(i)$ which
map each token~$t$ that triggers a jump with
respect to~$i$, to~$t$'s jump value (type~$J$).
\Cref{algorithm:coordination} is the algorithm for doing so.

\begin{algorithm}
  \begin{algorithmic}
  \caption{\label{algorithm:coordination}
    Function~$\Function Jumps(i)$ returning, for an item~$i∈I$,
  the dictionary~$d$ mapping each token~$t$ that
    triggers a jump with respect to~$i$, to~$t$'s jump value.
  }
  \LET{$[A::=α·Y₁…Yₙ]$}{$i$} \COMMENT{break~$i$ into components}
  \LET{$d$}{$∅$} \COMMENT{initialize return variable}
  \FOR[for all symbols in suffix of~$i$]{$j = 2,…,n$}
    \IF[continue to build~$d$?]{not~$\Function Nullable(Y₂…Y_{j-1})$}
      \BREAK
    \FI
    \FOR[symbols that might cause jump in~$Y₁$]{$t∈\Function First(Yⱼ)$}
      \IF[\nth{1} update of key~$t$ in dictionary~$d$?]{$d(t)=⊥$}
        \STATE{$i_{addr} = [A::=αY₁…·Yⱼ…Yₙ]$} \COMMENT{the jump address}
        \STATE{\bfseries\scriptsize{\texttt{// }\textit{handle the Push after Jump phenomena}}}
        \STATE{$d(t) = Consolidate(i_{addr},t)$} \COMMENT{update~$d(t)$}
      \FI
    \ENDFOR % Symbols
  \ENDFOR % Suffix
  \IF[Is it possible to jump beyond~$d$?]{not~$\Function Nullable(Y₂…Yₙ)$}
    \FOR[Don't allow a fallback after this point]{$t∈Σ$}
      \IF[Not a legal jump]{$d(t)=⊥$}
        \STATE{$d(t) = \textsc{Error}$}
      \FI
    \ENDFOR % Verbs
  \FI
  \RETURN{$d$}
  \end{algorithmic}
  \vspace{0.3ex}
  \hrule
  \vspace{0.3ex}
  \begin{itemize}
    \item \textsc{Error} is a special jump value that causes the \RLLp to reject instantly.
  \end{itemize}

\end{algorithm}

Let~$Y₁β= Y₁…Yₙ$ be the suffix denoted by~$i$'s dot.
Then, for each terminal~$t∈Σ$,
function~$\Function Jumps(i)$ determines the shortest
prefix~$Y₂…Y_{j-1}$ of~$β$ which is nullable
and such that~$t ∈ \Function First(Yⱼ)$.
The following line of thought explains the rationale.

Assume that~$Y₂…Y_{j-1}$ is nullable and
that~$t∈\Function First(Yⱼ)$, for some~$t\inΣ$.
Then, when the \RLLp finished parsing~$Y₁$ and~$t$
is seen, the \RLLp should conclude that~$Y₂…Y_{j-1}\stackrel * ⇒ε$
and jump forward to the point of the parsing process just before
seeing~$Yⱼ$.

This point in parsing is exactly the item obtained
from~$i$ by moving the dot to just before~$Yⱼ$.

One subtlety applies though.
In the case that the assumption holds for
both~$j$ and~$j'$,~$j < j'$ for the same token~$t$, there are
two alternatives to choose from:
\begin{itemize}
  \item~$Y₂…Y_{j-1}\stackrel * ⇒ε~$ and~$t$ is the first token in the derivation of~$Yⱼ$ i.e.,\[
  Yⱼ \stackrel * ⇒ tγ.
\]
\item~$Y₂…Y_{j'-1}\stackrel * ⇒ε~$ and~$t$ is the first token in the derivation~$Y_{j'}$ i.e.,\[
Y_{j'} \stackrel * ⇒ tγ'.
\]
\end{itemize}
Recalling that an \LLp pushes
stack symbols in reverse order and that \RLLp emulates its behavior, we can
see that~$Y_{j'}$ is never given the opportunity to derive~$t$.

After computing~$Yⱼ$, the algorithm uses~$\Function Consolidate(·,·)$
(defined in \cref{algorithm:consolidate}) to overcome the push after jump
phenomena mentioned in~\cref{subsection:push-after-jump}.

\subsection{Consolidating Push Operations}
\label{subsection:consolidate}
Recalling \cref{substitution-factor}, capturing the notion of
repeated substitution, we realize that the \RLLp-generator
must consolidate~$k'$ push operations into one.

Function~$\Function Consolidate(i,t)$ in \cref{algorithm:consolidate}
returns the consolidated list of consecutive push operations conducted
by the \RLLp parser in state~$i$ and encounters terminal~$t$.
This is achieved by figuring out, ahead-of-time,
the operations of the \LLp.

Function~$\Function Consolidate(·,·)$ is invoked by the \RLLp generator to
pre-compute the consolidated list of push operations for all relevant
item-token pairs. At runtime, the \RLLp will push the consolidated lists in
constant time, irrespective of the length of the consolidated list.

\begin{algorithm}[htb]
  \caption{\label{algorithm:consolidate}
    Function~$\Function Consolidate(i,t)$ pre-computing~$L$, the list of push
    operations that happen when an item~$i$ at the top of an \RLLp's stack
    encounters terminal~$t∈Σ∪❴\$❵$ on the input.
  }
  \begin{algorithmic}
    \STATE{$[X::=α·Yβ] ← i$} \COMMENT{break~$i$ into its components}
    \LET{$L$}{$∅$} \COMMENT{initialize return value}
    \WHILE[loop while~$Y$ is a nonterminal]{$Y∉Σ$}
      %\STATE{$\Function print(r)$} \COMMENT{useful for parsing}
      \STATE{$\Function push(L,[X ::=α·Yβ])$}\COMMENT{currently parsing~$Y$}
      \STATE{$r←\Function predict(Y,t)$} \COMMENT{next rule to apply}
      \STATE{$[Y::=X₁…Xₘ]←r$} \COMMENT{break~$r$ into components}
      \IF[$\Function predict(·,·)$ returned an~$ε$-rule]{$X₁…Xₘ=ε$}
        \WHILE[pop all exhausted rules]{$\Function exhausted(\Function peek(L))$}
          \STATE{$\Function pop(L)$}
        \ENDWHILE
        \STATE{$[X::=α·Yβ] ← \Function pop(L)$} \COMMENT{$Y$ was just fully parsed}
        \STATE{$\Function push(L,[X ::=αY·β])$} \COMMENT{advance the rule}
      \ELSE[$\Function predict(·,·)$ returned a non~$ε$-rule]
        \STATE{$\Function push(L,[Y::=·X₁…Xₘ])$} \COMMENT{we now turn to parse~$r$}
      \FI
      \STATE{$[X::=α·Yβ] ← \Function pop(L)$} \COMMENT{break~$i$ into its components}
    \ENDWHILE
    \STATE{$\Function push(L,[X ::=αY·β])$} \COMMENT{$Y$ must be~$t$}
    \RETURN{$L$} \COMMENT{return the items to push}
  \end{algorithmic}
  \vspace{0.3ex}
  \hrule
  \vspace{0.3ex}
  \small
  \begin{itemize}
    \item List~$L$ is used in the main loop of the code to emulate the
          runtime stack of the generated \RLLp, and thus, pre-compute the net effect
          of stack operations that take place in
          configuration~$⟨i,t⟩$ until~$t$ is consumed.
          \par
          Accordingly, the emulation applies stack
          functions~$\Function pop(·)$ and~$\Function peek(·)$
          as well as operation~$\Function push(·,·)$ on the list~$L$,
          as if it were a stack.
          \par
          Calling~$\Function (·)$ on the emulation stack~$L$ at the time an
          \RLLp is generated, obviates the need for the \RLLp to~$\Function
          peek()$, at runtime, into its own stack.
    \item
          The algorithm relies on function~$\Function exhausted(i)$ that returns
          true for an item~$i$ when its dot is in its penultimate position,
          i.e., the item represents times during parsing in which all
          right hand side symbols of the item's rule have been parsed except for the last.
          Since the item was just revealed at the top of the stack, the last symbol
          was parsed as well, and the rule is fully parsed (and thus, exhausted).
          \par
          Intuitively, an item~$i$ is exhausted right after the rule's body
          have been seen in full, and the item ‟waits” for the \RLLp to take
          the stack action appropriate when the rule reduces, and items representing
          it are no longer need.
  \end{itemize}
\end{algorithm}


\subsection{Putting the Pieces Together}
Generating an \RLLp for a given LL grammar requires providing to the
built-in algorithm of the \RLLp, the specific information it needs to
realize the given grammar.

Reexamining the code of the \RLLp (\cref{algorithm:rll-parser})
we see that all such information is contained in
function~$Δ$.
Therefore, the core of the \RLLp generator is
\cref{algorithm:main} that computes~$Δ$.

\begin{algorithm}
  \caption{\label{algorithm:main}
    Compute contents of prediction table (transition function) entry~$Δ[i,t]$
  for all item~$i∈I$, token~$t ∈Σ$ pairs for which this entry is defined. }

  \begin{algorithmic}
    \FOR[for each item]{$i∈I$}
    \LET{$[A::=α·Yβ]$}{$i$} \COMMENT{break~$i$ into components}
      \IF[$Δ$ doesn't handle terminals]{$Y∈Σ$}
        \CONTINUE \COMMENT{handle next item.}
      \FI
      \FOR[calculate entry~${Δ[i,t]}$]{$t∈Σ$}
        \IF[$t$ is consumed while parsing~$i$]{$t∈\Function First(Yβ)$}
          \LET{$L$}{$\Function Consolidate(i,t)$}
          \LET{$Δ[i,t]$}{$\textcolor{red!70!black}{\texttt{[}}\Function push(L)\textcolor{red!70!black}{\texttt{]}}$} \COMMENT{a push operation}
        \ELSIF[$t$ is consumed after parsing~$i$]{$t∈\Function Follow(A)$ and~$\Function Nullable(Yβ)$}
          \STATE{\bfseries\scriptsize{\texttt{// }\textit{Obtain jumps dictionary and store in~$Δ$:}}}
          \LET{$Δ[i,t]$}{$\textcolor{red!70!black}{\texttt{[}}\Function jump(t)\textcolor{red!70!black}{\texttt{]}}$} \COMMENT{a jump operation}
        \FI
      \ENDFOR % for all terminals
    \ENDFOR % for all items
  \end{algorithmic}
\end{algorithm}

Recall that we manage the ‟$k^*$” phenomena using jumps, and that the
jumps are realized by the sophisticated JSM data structure that drives the
\RLLp.

The contract between the \RLLp and its generator is that function~$Δ$
supplies the dictionaries that need to be pushed (and later jumped to)
by the JSM.

This information is retrieved by the \RLLp from the~$Δ$ table,
and used and~$\Function push(·)$ed accordingly into the JSM.
Only with the aide of this information, the underlying JSM can support the
constant-time jumps (lines 5 and 25).

For this reason, \cref{algorithm:main} implicitly consults the function~$\Function Jumps(·)$
(recall that~$\Function Jumps(·)$ is the coordinator between stacks~$S₀$ and~$S₁$)
to compute this dictionary storing it in the~$Δ$ table.

Another part of the contract between the parser and its generator deals
with the~‟$k'$” phenomena. The generator invokes function~$\Function
Consolidate(·)$ (\cref{algorithm:consolidate}) to
compute~$L$, the consolidated list of push operations. List~$L$ will be later
read by \RLLp (line 18 in \cref{algorithm:rll-parser}).
