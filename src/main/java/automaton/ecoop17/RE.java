package automaton.ecoop17;

import static automaton.ecoop17.RE.NT.*;
import static automaton.ecoop17.RE.Term.*;

import org.spartan.fajita.api.Fajita;
import org.spartan.fajita.api.Main;
import org.spartan.fajita.api.bnf.symbols.NonTerminal;
import org.spartan.fajita.api.bnf.symbols.Terminal;
import org.spartan.fajita.api.junk.Chars;
import org.spartan.fajita.api.junk.NoDuplRe;
import static org.spartan.fajita.api.junk.Regex.*;

public class RE {
  // begin{terminals}
  static enum Term implements Terminal {
    moreThanZero, zeroOrMore, atLeast, times, atMost, //
    anyChar, str, alphanumeric, alphabetic, chars, chr, to //
    , oneOf, notOneOf //
  }
  // end{terminals}
  // begin{nonterminals}

  static enum NT implements NonTerminal {
    RE, OPT_RE, SIMPLE_RE, //
    NO_DUPL_RE, QUANTIFIER, OPT_QUANTIFIER, //
    SET, CHARS
  }

  // end{nonterminals}
  public static void buildBNF() {
    // begin{fajita}
    Fajita.buildBNF(Term.class, NT.class) //
        .setApiName("Regex") //
        .start(RE) //
        // begin{grammar}
        .derive(RE).to(SIMPLE_RE).and(OPT_RE) //
        .derive(OPT_RE).to(RE) //
        /* */.orNone() //
        .derive(SIMPLE_RE).to(NO_DUPL_RE).and(OPT_QUANTIFIER)//
        /* */.or(zeroOrMore, NO_DUPL_RE)//
        /* */.or(moreThanZero, NO_DUPL_RE)//
        .derive(OPT_QUANTIFIER).to(QUANTIFIER) //
        /* */.orNone() //
        .derive(QUANTIFIER).to(atLeast, int.class).and(times)//
        /* */.or(atMost, int.class).and(times)//
        .derive(NO_DUPL_RE).to(str, String.class)//
        /* */.or(anyChar)//
        /* */.or(SET)//
        .derive(SET).to(oneOf, CHARS)//
        /* */.or(notOneOf, CHARS)//
        .derive(CHARS).to(chars, Fajita.ellipsis(char.class)) //
        /* */.or(chr, char.class).and(to, char.class) //
        /* */.or(alphanumeric)//
        /* */.or(alphabetic)//
        // end{grammar}
        .go(Main.packagePath);
    // end{fajita}
  }
  static void test() {
    // start{legal}
    oneOf(Chars.alphabetic()).zeroOrMore(NoDuplRe.oneOf(Chars.alphanumeric()));
    str("5times").atLeast(5).times().str("less").atMost(4).times();
    // end{legal}
    // start{illegal}
    oneOf(NoDuplRe.oneOf(Chars.alphabetic()));
    str("5times").atLeast(5).str("less");
    // end{illegal}
  }
}
