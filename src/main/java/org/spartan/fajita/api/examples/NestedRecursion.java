package org.spartan.fajita.api.examples;

import static org.spartan.fajita.api.examples.NestedRecursion.NT.*;
import static org.spartan.fajita.api.examples.NestedRecursion.Term.*;
import static org.spartan.fajita.api.junk.Balancedparent.a;

import java.io.IOException;
import java.util.Map;

import org.spartan.fajita.api.Fajita;
import org.spartan.fajita.api.Main;
import org.spartan.fajita.api.bnf.symbols.NonTerminal;
import org.spartan.fajita.api.bnf.symbols.Terminal;
import org.spartan.fajita.api.junk.Utils.ASTNode;

public class NestedRecursion {
  private static final String apiName = "BalancedParent";

  static enum Term implements Terminal {
    a, b, end;
  }

  static enum NT implements NonTerminal {
    A, B, B2, S, F
  }

  public static Map<String, String> buildApi() {
    return Fajita.buildBNF(Term.class, NT.class) //
        .setApiName(apiName) //
        .start(F) //
        .derive(F).to(S).and(end)//
        .derive(S).to(A).and(S).and(B) //
        /*    */.orNone()//
        .derive(A).to(a) //
        .derive(B).to(B2).and(b) //
        .derive(B2).to(b) //
        .go(Main.packagePath);
  }
  public static void main(String[] args) throws IOException {
    Main.apiGenerator(buildApi());
  }
  @SuppressWarnings("unused") static void test() {
    ASTNode $;
    $ = a().b().b().end();
    $ = a().a().b().b().b().b().end();
    $ = a().a().a().b().b().b().b().b().b().end();
  }
}
